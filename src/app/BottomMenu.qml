import QtQuick 2.9
import QtQuick.Controls 2.5 as QQC2
import QtQuick.Layouts 1.12
import Ubuntu.Components 1.3

QQC2.Menu {
    id: bottomMenu

    property alias model: actionsRepeater.model

    parent: QQC2.ApplicationWindow.overlay
    y: parent.height - height
    margins: units.gu(2)

    Rectangle {
        id: bottomActionsContent

        color: "transparent"
        height: actionsRow.height
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        RowLayout {
            id: actionsRow

            anchors {
                bottom: parent.bottom
                left: parent.left
                right: parent.right
            }
            spacing: 0

            Repeater {
                id: actionsRepeater

                model: bottomMenu.bottomActions
                delegate: toolButtonDelegate
            }

            Component {
                id: toolButtonDelegate

                QQC2.ToolButton {
                    id: toolButton

                    property var contextMenu: modelData.contextMenu

                    Layout.fillWidth: true
                    implicitHeight: units.gu(7)
                    enabled: modelData.enabled
                    visible: modelData.visible || modelData.alwaysDisplayAtBottom
                    icon.name: modelData.iconName

                    Connections {
                        target: contextMenu
                        onVisibleChanged: if (!target.visible) highlighted = false
                    }

                    onPressAndHold: {
                        highlighted = true
                        modelData.showContextMenu(true, toolButton)
                    }
                    onClicked: {
                        modelData.triggerAction(true, toolButton)
                        if (modelData.closeMenuOnTrigger) {
                            bottomMenu.close()
                        }
                    }
                }
            }
        }
    }
}
