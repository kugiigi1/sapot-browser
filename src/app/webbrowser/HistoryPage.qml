import QtQuick 2.4
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.12
import webbrowserapp.private 0.1
import "Highlight.js" as Highlight
import "../pageheader" as PageHeader
import ".." as Common
import "." as Local

Common.BasePage {
    id: historyPage

    property bool searchMode: false
    readonly property bool selectMode: urlsListView.ViewItems.selectMode
    readonly property real filterPaneWidth: units.gu(30)
    readonly property bool filterShownInNarrow: filterPane.shown && !wide

    signal historyEntryClicked(url url)

    title: i18n.tr("History")
    allowRightOverflow: false
    wide: width >= filterPaneWidth * 3
    showBackButton: !filterShownInNarrow
    headerLeftActions: historyPage.displayedInFullWidthPanel && !filterPane.shown ? [ ] : [ filterAction ]
    headerRightActions: historyPage.displayedInFullWidthPanel
                                && !filterPane.shown ? [ filterAction, searchAction, deleteAction, selectAllAction, cancelAction, selectModeAction ]
                                    : [ searchAction, deleteAction, selectAllAction, cancelAction, selectModeAction ]
    
    Common.BaseAction {
        id: searchAction

        text: i18n.tr("Search")
        tooltipText: i18n.tr("Search history")
        iconName: "find"
        visible: !historyPage.selectMode && !historyPage.filterShownInNarrow
        shortcut: historyPage.enableShortcuts ? StandardKey.Find : ""

        onTrigger: {
            historyPage.searchMode = true
            searchQuery.forceActiveFocus()
        }
    }

    Common.BaseAction {
        id: cancelAction

        text: i18n.tr("Exit mode")
        tooltipText: i18n.tr("Exit selection mode")
        iconName: "cancel"
        visible: historyPage.selectMode && !historyPage.filterShownInNarrow

        onTrigger: urlsListView.ViewItems.selectMode = false
    }

    Common.BaseAction {
        id: deleteAction

        text: i18n.tr("Delete items")
        tooltipText: i18n.tr("Delete selected items")
        iconName: "delete"
        visible: historyPage.selectMode && urlsListView.ViewItems.selectedIndices.length > 0
                        && !historyPage.filterShownInNarrow

        onTrigger: internal.removeSelected()
    }

    Common.BaseAction {
        id: selectModeAction

        text: i18n.tr("Selection mode")
        tooltipText: i18n.tr("Enter selection mode")
        iconName: "edit"
        visible: !historyPage.selectMode && !historyPage.filterShownInNarrow
        enabled: urlsListView.count > 0 && visible

        onTrigger: urlsListView.ViewItems.selectMode = true
    }

    Common.BaseAction {
        id: selectAllAction

        text: urlsListView.allItemsSelected ? i18n.tr("Deselect all") : i18n.tr("Select all")
        tooltipText: urlsListView.allItemsSelected ? i18n.tr("Deselect all items") : i18n.tr("Select all items")
        iconName: urlsListView.allItemsSelected ? "select-none" : "select"
        visible: historyPage.selectMode && !historyPage.filterShownInNarrow
        enabled: urlsListView.count > 0

        onTrigger: internal.toggleSelectAll()
    }

    Common.BaseAction {
        id: filterAction

        text: filterPane.shown ? i18n.tr("Hide filters") : i18n.tr("Show filters")
        tooltipText: filterPane.shown ? i18n.tr("Go back") : i18n.tr("Show panel for available filters")
        iconName: filterPane.shown ? "back" : "filters"
        visible: !historyPage.wide
        shortcut: historyPage.enableShortcuts && filterPane.shown ? StandardKey.Cancel : "Ctrl+E"

        onTrigger: toggleFilterPanel()
    }
    
    function toggleFilterPanel() {
        filterPane.shown = !filterPane.shown
    }

    Keys.onLeftPressed: {
        if (!wide && !filterPane.shown) {
            toggleFilterPanel()
        }
        lastVisitDateListView.forceActiveFocus()
    }
    Keys.onRightPressed: {
        if (!wide && filterPane.shown) {
            toggleFilterPanel()
        }
        if (urlsListView.currentIndex == -1) {
            urlsListView.currentIndex = 0
        }
        urlsListView.forceActiveFocus()
    }
    Keys.onUpPressed: {
        searchMode = true
        searchQuery.forceActiveFocus()
    }

    Keys.onDeletePressed: {
        if (urlsListView.ViewItems.selectMode) {
            internal.removeSelected()
        } else {
            if (urlsListView.activeFocus) {
                HistoryModel.removeEntryByUrl(urlsListView.currentItem.siteUrl)

                if (urlsListView.count == 0) {
                    lastVisitDateListView.currentIndex = 0
                }
            } else {
                if (lastVisitDateListView.currentIndex == 0) {
                    HistoryModel.clearAll()
                } else {
                    HistoryModel.removeEntriesByDate(lastVisitDateListView.currentItem.lastVisitDate)
                    lastVisitDateListView.currentIndex = 0
                }
            }
        }
    }

    onWideChanged: filterPane.shown = wide
    onSelectModeChanged: if (selectMode) searchMode = false

    Timer {
        // Set the model asynchronously to ensure
        // the view is displayed as early as possible.
        id: loadModelTimer
        interval: 1
        onTriggered: historySearchModel.sourceModel = HistoryModel
    }

    function loadModel() { loadModelTimer.restart() }

    TextSearchFilterModel {
        id: historySearchModel
        searchFields: ["title", "url"]
        terms: searchQuery.terms
    }

    RowLayout {
        id: contentRow

        anchors.fill: parent
        spacing: 0

        FocusScope {
            id: filterPane

            property bool shown: false

            Layout.preferredWidth: historyPage.wide ? units.gu(30) : 0
            Layout.fillHeight: true
            z: 1

            onShownChanged: {
                if ((!shown && !historyPage.wide) || (shown && historyPage.wide)) {
                    urlsListView.forceActiveFocus()
                }
            }

            Rectangle {
                anchors {
                    top: parent.top
                    left: parent.left
                    leftMargin: filterPane.shown ? 0 : -width
                    right: historyPage.wide ? parent.right : undefined
                    bottom: parent.bottom
                    Behavior on leftMargin { UbuntuNumberAnimation { duration: UbuntuAnimation.SnapDuration } }
                }
                width: historyPage.wide ? parent.width : contentRow.width
                color: theme.palette.normal.background

                Common.BaseListView {
                    id: lastVisitDateListView
                    objectName: "lastVisitDateListView"

                    anchors {
                        fill: parent
                        topMargin: units.gu(1)
                        bottomMargin: anchors.topMargin
                    }
                    focus: true
                    pageHeader: historyPage.pageManager ? historyPage.pageManager.header : historyPage.header

                    currentIndex: 0
                    onCurrentIndexChanged: urlsListView.ViewItems.selectedIndices = []

                    model: HistoryLastVisitDateListModel {
                        sourceModel: historyLastVisitDateModel.model
                    }
                    
                    delegate: Common.NavigationDelegate {
                        id: lastVisitDateDelegate
                        objectName: "lastVisitDateDelegate"

                        property var lastVisitDate: model.lastVisitDate

                        anchors {
                            left: parent.left
                            right: parent.right
                            margins: units.gu(1)
                        }

                        text: {
                            if (!lastVisitDate.isValid()) {
                                return i18n.tr("All History")
                            }

                            var today = new Date()
                            today.setHours(0, 0, 0, 0)

                            var yesterday = new Date()
                            yesterday.setDate(yesterday.getDate() - 1)
                            yesterday.setHours(0, 0, 0, 0)

                            var entryDate = new Date(lastVisitDate)
                            entryDate.setHours(0, 0, 0, 0)

                            if (entryDate.getTime() == today.getTime()) {
                                return i18n.tr("Today")
                            } else if (entryDate.getTime() == yesterday.getTime()) {
                                return i18n.tr("Yesterday")
                            }
                            return Qt.formatDate(lastVisitDate, Qt.DefaultLocaleLongDate)
                        }

                        highlighted: ListView.isCurrentItem

                        onClicked: {
                            ListView.view.currentIndex = index
                            if (historyPage.filterShownInNarrow) {
                                historyPage.toggleFilterPanel()
                            }
                        }

                        ListView.onRemove: {
                            if (ListView.isCurrentItem) {
                                // For some reason, setting the current index here
                                // results in it being reset to its previous value
                                // right away. Delaying it with a timer so the
                                // operation is queued does the trick.
                                resetIndexTimer.restart()
                            }
                        }
                    }

                    Timer {
                        id: resetIndexTimer
                        interval: 0
                        onTriggered: lastVisitDateListView.currentIndex = 0
                    }
                }

                Keys.onUpPressed: {
                    if (searchMode) {
                        searchQuery.forceActiveFocus()
                    } else {
                        event.accepted = false
                    }
                }

                Scrollbar {
                    flickableItem: lastVisitDateListView
                    align: Qt.AlignTrailing
                }
            }
        }

        Rectangle {
            Layout.preferredWidth: units.dp(2)
            Layout.fillHeight: true
            color: theme.palette.normal.foreground
            visible: historyPage.wide
        }

        ColumnLayout {
            Layout.fillWidth: true
            Layout.fillHeight: true

            Item {
                id: searchBar

                Layout.fillWidth: true
                Layout.topMargin: historyPage.searchMode ? 0 : -height
                Layout.preferredHeight: units.gu(7)

                Behavior on Layout.topMargin { UbuntuNumberAnimation { duration: UbuntuAnimation.SnapDuration } }

                TextField {
                    id: searchQuery
                    objectName: "searchQuery"

                    anchors {
                        left: parent.left
                        right: parent.right
                        margins: units.gu(2)
                        verticalCenter: parent.verticalCenter
                    }
                    inputMethodHints: Qt.ImhNoPredictiveText
                    primaryItem: Icon {
                       height: parent.height - units.gu(2)
                       width: height
                       name: "search"
                    }
                    hasClearButton: true
                    placeholderText: i18n.tr("Search history")
                    readonly property var terms: text.split(/\s+/g).filter(function(term) { return term.length > 0 })

                    Keys.onDownPressed: urlsListView.forceActiveFocus()
                    Keys.onEscapePressed: historyPage.searchMode = false
                }
            }

            Item {
                Layout.fillWidth: true
                Layout.fillHeight: true

                Common.BaseListView {
                    id: urlsListView
                    objectName: "urlsListView"

                    readonly property bool allItemsSelected: ViewItems.selectedIndices.length === count

                    anchors.fill: parent
                    currentIndex: -1
                    pageHeader: historyPage.pageManager ? historyPage.pageManager.header : historyPage.header

                    model: SortFilterModel {
                        id: historyLastVisitDateModel
                        readonly property date lastVisitDate: lastVisitDateListView.currentItem ? lastVisitDateListView.currentItem.lastVisitDate : ""
                        filter {
                            property: "lastVisitDateString"
                            pattern: new RegExp(lastVisitDate.isValid() ? "^%1$".arg(Qt.formatDate(lastVisitDate, "yyyy-MM-dd")) : "")
                        }
                        // Until a valid HistoryModel is assigned the TextSearchFilterModel
                        // will not report role names, and the HistoryLastVisitDateListModel
                        // will emit warnings since it needs a dateLastVisit role to be
                        // present.
                        model: historySearchModel.sourceModel ? historySearchModel : null
                    }

                    clip: true

                    onModelChanged: urlsListView.currentIndex = -1
                    onMovingChanged: {
                        if (moving && searchQuery.text.trim() == "") {
                            historyPage.searchMode = false
                        }
                    }

                    // Show sections when page is not in wide layout
                    section.property: historyPage.wide ? "" : "lastVisitDate"
                    section.delegate: HistorySectionDelegate {
                        width: parent.width - units.gu(3)
                        anchors.left: parent.left
                        anchors.leftMargin: units.gu(2)
                        todaySectionTitle: i18n.tr("Today")
                    }

                    delegate: UrlDelegate{
                        objectName: "historyDelegate"
                        width: parent.width - units.gu(1)
                        height: units.gu(5)

                        property url siteUrl: model.url

                        icon: model.icon
                        title: Highlight.highlightTerms(model.title ? model.title : model.url, searchQuery.terms, theme.palette.normal.focus)
                        url: Highlight.highlightTerms(model.url, searchQuery.terms, theme.palette.normal.focus)

                        headerComponent: Label {
                            text: Qt.formatTime(model.lastVisit)
                            textSize: Label.XSmall
                        }

                        onClicked: {
                            if (selectMode) {
                                selected = !selected
                            } else {
                                historyPage.historyEntryClicked(model.url)
                            }
                        }

                        onRemoved: {
                            HistoryModel.removeEntryByUrl(model.url)
                            if (urlsListView.count == 0) {
                                lastVisitDateListView.currentIndex = 0
                            }
                        }

                        onPressAndHold: {
                            if (historyPage.searchMode) return
                            selectMode = !selectMode
                            if (selectMode) {
                                urlsListView.ViewItems.selectedIndices = [index]
                            }
                        }
                    }
                }

                Scrollbar {
                    flickableItem: urlsListView
                    align: Qt.AlignTrailing
                }
            }
        }
    }

    QtObject {
        id: internal

        function toggleSelectAll() {
            if (urlsListView.allItemsSelected) {
                urlsListView.ViewItems.selectedIndices = []
            } else {
                var indices = []
                for (var i = 0; i < urlsListView.count; ++i) {
                    indices.push(i)
                }
                urlsListView.ViewItems.selectedIndices = indices
            }

            urlsListView.forceActiveFocus()
        }

        function removeSelected() {
            var indices = urlsListView.ViewItems.selectedIndices
            var urls = []
            for (var i in indices) {
                urls.push(urlsListView.model.get(indices[i])["url"])
            }

            if (urlsListView.count == urls.length) {
                lastVisitDateListView.currentIndex = 0
            }

            urlsListView.ViewItems.selectMode = false
            for (var j in urls) {
                HistoryModel.removeEntryByUrl(urls[j])
            }

            lastVisitDateListView.forceActiveFocus()
        }
    }
}
