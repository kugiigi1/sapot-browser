import QtQuick 2.4
import Ubuntu.Components 1.3

Item {
    id: lastUsedTabPreview

    property var tabData
    property alias incognito: chrome.incognito
    property real chromeHeight
    property bool showChrome: true
    property bool dragging: false
    opacity: 0

    function show(delayed) {
        if (delayed) {
            // Delay showing so that it won't show when opening tabs list directly with a quick swipe
            delayShowTimer.restart()
        } else {
            opacity = 1
        }
    }

    function hide() {
        delayShowTimer.stop()
        opacity = 0
    }

    Behavior on opacity {
        enabled: dragging
        UbuntuNumberAnimation {
            duration: UbuntuAnimation.FastDuration
        }
    }

    Timer {
        id: delayShowTimer
        running: false
        interval: 200
        onTriggered: opacity = 1
    }

    TabChrome {
        id: chrome

        visible: lastUsedTabPreview.showChrome
        anchors {
            bottom: parent.top
            left: parent.left
            right: parent.right
        }
        title: tabData.title ? tabData.title
                             : (tabData.url.toString() ? tabData.url
                                                       : i18n.tr("New tab"))
        icon: tabData.icon
        showCloseIcon: false
        tabWidth: lastUsedTabPreview.width
        height: lastUsedTabPreview.chromeHeight
        loading: lastUsedTabPreview.tabData.tab
                        && lastUsedTabPreview.tabData.tab.webview
                        && lastUsedTabPreview.tabData.tab.webview.loading
                        && lastUsedTabPreview.tabData.tab.webview.loadProgress !== 100
    }

    Item {
        anchors {
            top: chrome.bottom
            topMargin: units.dp(-1)
            left: parent.left
            right: parent.right
        }

        height: parent.height
        clip: true

        Rectangle {
            anchors.fill: parent
            color: theme.palette.normal.foreground
        }
        
        Rectangle {
            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }
            height: units.dp(1)

            color: theme.palette.normal.base
        }

        Image {
            visible: !previewContainer.visible
            source: "assets/tab-artwork.png"
            asynchronous: true
            fillMode: Image.PreserveAspectFit
            width: parent.width / 5
            height: width
            anchors {
                right: parent.right
                rightMargin: -width / 5
                bottom: parent.bottom
                bottomMargin: -height / 10
            }
        }

        Image {
            id: previewContainer
            visible: source.toString() && (status == Image.Ready)
            anchors.fill: parent
            anchors.topMargin: units.dp(1)
            verticalAlignment: Image.AlignTop
            fillMode: Image.PreserveAspectCrop
            source: lastUsedTabPreview.tabData.tab ? lastUsedTabPreview.tabData.tab.preview : ""
            asynchronous: true
            cache: false
        }
    }
}
