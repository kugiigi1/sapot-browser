import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12 as QQC2
import QtQuick.Controls.Suru 2.2
import Qt.labs.settings 1.0
import QtQml.Models 2.2
import Ubuntu.Components 1.3
import QtWebEngine 1.5
import Morph.Web 0.1
import webbrowserapp.private 0.1
import webbrowsercommon.private 0.1
import ".." as Common
import "../TextUtils.js" as TextUtils

Common.BasePage {
    id: settingsPage

    property QtObject settingsObject
    property bool searchMode: false
    readonly property bool searchIsActive: searchMode && searchQuery.text !== ""
    readonly property real filterPaneWidth: units.gu(30)
    readonly property bool notInMainPageNarrow: !filterPane.shown && !wide

    wide: width >= filterPaneWidth * 3
    showBackButton: !notInMainPageNarrow

    headerLeftActions: [ mainBackAction ]
    headerRightActions: [ searchAction, cancelSearchAction ]

    title: searchIsActive ? i18n.tr("Search: %1").arg(filteredModel.currentGroupTitle)
                          : wide || filterPane.shown ? i18n.tr("Settings") : filteredModel.currentGroupTitle
    allowRightOverflow: false

    signal clearCache(var sig_itemsToDelete)
    signal clearAllCookies()
    signal done()

    onWideChanged: {
        filterPane.shown = wide
    }

    onSearchModeChanged: {
        if (searchMode) {
            if (!wide && filterPane.shown) {
                // Always set to All when narrow and group list page is shown upon entering search mode
                settingsGroupListView.currentIndex = 0
            }
        } else {
            searchQuery.clear()
        }
    }

    onSearchIsActiveChanged: {
        if (searchIsActive) {
            settingsGroupListView.currentIndex = 0
        }
    }

    function toggleFilterPanel() {
        filterPane.shown = !filterPane.shown
    }

    Common.BaseAction {
        id: searchAction

        text: i18n.tr("Search")
        tooltipText: i18n.tr("Search available settings")
        iconName: "find"
        shortcut: settingsPage.enableShortcuts ? StandardKey.Find : ""

        onTrigger: {
            if (!wide) {
                filterPane.shown = false
            }
            settingsPage.searchMode = true
            searchQuery.forceActiveFocus()
        }
    }

    Common.BaseAction {
        id: cancelSearchAction

        visible: settingsPage.searchMode
        text: i18n.tr("Cancel search")
        tooltipText: i18n.tr("Exit search")
        iconName: "cancel"

        onTrigger: settingsPage.searchMode = false
    }

    Common.BaseAction {
        id: mainBackAction

        text: i18n.tr("Back")
        tooltipText: i18n.tr("Go back")
        iconName: "back"
        visible: !settingsPage.wide && !filterPane.shown
        shortcut: settingsPage.enableShortcuts && !filterPane.shown ? StandardKey.Cancel : "Ctrl+E"

        onTrigger: filterPane.shown = true
    }

    SearchEngines {
        id: searchEngines
        searchPaths: searchEnginesSearchPaths
    }

    DelegateModel {
        id: filteredModel

        property string currentGroupId
        property string currentGroupTitle

        model: settingsItemsModel
        
        onCurrentGroupIdChanged: filterByGroup(currentGroupId)

        function getHeaderAndHigherLevels(_itemIndex, _itemLevel) {
            let _result = { "header": null, "upperLevels": [] }

            for (let i = _itemIndex; i >= 0; --i) {
                let _item = items.get(i);
                let _itemType = _item.model.itemType
                let _currentItemLevel = _item.model.itemLevel

                if (_itemType == Common.SettingsItem.ItemType.Header && !_result.header) {
                    _result.header = _item
                }

                if (_itemLevel > 0 && _itemType !== Common.SettingsItem.ItemType.Header
                        // Haven't identified all upper levels yet and current item's level is lower than the input item level
                        && _result.upperLevels.length < _itemLevel && _currentItemLevel < _itemLevel
                        // Current item's level is lower than the lowest level added to result
                        && (
                                _result.upperLevels.length == 0
                                            || (_result.upperLevels.length > 0
                                                    && _currentItemLevel < _result.upperLevels[_result.upperLevels.length - 1].model.itemLevel)
                            )
                    ) {
                    _result.upperLevels.push(_item)
                }
            }

            return _result
        }

        function update(searchText) {
            console.log("Search settings: " + searchText)
            if (items.count > 0) {
                items.removeGroups(0, items.count, ["match"]);
            }

            if (searchText) {
                var match = [];
                var searchTextUpper = searchText.toUpperCase()
                var titleUpper
                var descrUpper
                var item
                var itemType
                var itemMainGroupUpper
                var itemSubGroupUpper

                for (var i = 0; i < items.count; ++i) {
                    item = items.get(i);
                    itemType = item.model.itemType

                    if (itemType !== Common.SettingsItem.ItemType.Header && item.model.visible) { //Exlude headers in search results
                        titleUpper = item.model.title.toUpperCase()
                        descrUpper = item.model.description.toString().toUpperCase()
                        itemMainGroupUpper = item.model.mainGroup.toString().toUpperCase()
                        itemSubGroupUpper = item.model.subGroup.toString().toUpperCase()

                        if (titleUpper.indexOf(searchTextUpper) > -1
                                || descrUpper.indexOf(searchTextUpper) > -1
                                || itemMainGroupUpper.indexOf(searchTextUpper) > -1
                                || itemSubGroupUpper.indexOf(searchTextUpper) > -1) { 
                            match.push({"item": item, "itemIndex": i });
                        }
                    }
                }

                for (let h = 0; h < match.length; ++h) {
                    let _item = match[h].item;
                    _item.inMatch = true;

                    // Include headers and higher levels of results
                    let higherLevelsAndHeader = getHeaderAndHigherLevels(match[h].itemIndex, _item.model.itemLevel)
                    if (higherLevelsAndHeader.header) {
                        higherLevelsAndHeader.header.inMatch = true
                    }

                    if (higherLevelsAndHeader.upperLevels.length > 0) {
                        for (let k = 0; k < higherLevelsAndHeader.upperLevels.length; ++k) {
                            higherLevelsAndHeader.upperLevels[k].inMatch = true
                        }
                    }
                }
            } else {
                filterByGroup(currentGroupId)
            }
        }

        function filterByGroup(_groupId) {
            if (items.count > 0) {
                items.removeGroups(0, items.count, ["group"]);
            }

            if (_groupId) {
                var _match = [];
                var _item

                for (var i = 0; i < items.count; ++i) {
                    _item = items.get(i);

                    if ((_item.model.mainGroup == _groupId || _groupId == "all")
                            && _item.model.visible) {
                        _match.push(_item);
                    }
                }

                for (i = 0; i < _match.length; ++i) {
                    _item = _match[i];
                    _item.inGroup = true;
                }
            }
        }

        groups: [
            DelegateModelGroup {
                id: matchGroup

                name: "match"
                includeByDefault: false
            }
            , DelegateModelGroup {
                id: settingGroup

                name: "group"
                includeByDefault: false
            }
        ]

        delegate: Loader {
            id: itemLoader

            readonly property real leftIndentionMargin: model.itemLevel * units.gu(2)

            property var modelData: model

            Layout.fillWidth: true
            Layout.leftMargin: leftIndentionMargin + units.gu(1)
            Layout.rightMargin: units.gu(1)

            visible: model.visible && ((!settingsPage.searchIsActive && DelegateModel.inGroup)
                                            || (settingsPage.searchIsActive && DelegateModel.inMatch && DelegateModel.inGroup))
            asynchronous: true
            sourceComponent: model && model.itemComponent ? model.itemComponent
                                : settingsItemFactory.getComponent(model.itemType)
        }
    }

    RowLayout {
        id: contentRow

        anchors.fill: parent
        spacing: 0

        FocusScope {
            id: filterPane

            property bool shown: true

            Layout.preferredWidth: settingsPage.wide ? units.gu(30) : 0
            Layout.fillHeight: true
            z: 1

            onShownChanged: {
                if ((!shown && !settingsPage.wide) || (shown && settingsPage.wide)) {
                    mainScrollView.forceActiveFocus()
                }

                if (shown) {
                    settingsPage.searchMode = false
                }
            }

            Rectangle {
                anchors {
                    top: parent.top
                    left: parent.left
                    leftMargin: filterPane.shown ? 0 : -width
                    right: settingsPage.wide ? parent.right : undefined
                    bottom: parent.bottom
                    Behavior on leftMargin { UbuntuNumberAnimation { duration: UbuntuAnimation.SnapDuration } }
                }
                width: settingsPage.wide ? parent.width : contentRow.width
                color: theme.palette.normal.background

                Common.BaseListView {
                    id: settingsGroupListView
                    objectName: "settingsGroupListView"

                    anchors {
                        fill: parent
                        topMargin: units.gu(1)
                        bottomMargin: anchors.topMargin
                    }
                    focus: true
                    pageHeader: settingsPage.pageManager ? settingsPage.pageManager.header : settingsPage.header
                    currentIndex: -1

                    onCurrentIndexChanged: {
                        filteredModel.currentGroupId = model[currentIndex].mainGroup
                        filteredModel.currentGroupTitle = model[currentIndex].title
                    }
                    Component.onCompleted: currentIndex = 0

                    model: settingsPage.settingsMainGroupsModel

                    delegate: Common.NavigationDelegate {
                        id: settingsGroupDelegate
                        objectName: "settingsGroupDelegate"

                        anchors {
                            left: parent.left
                            right: parent.right
                            margins: units.gu(1)
                        }

                        text: model.title
                        icon.name: model.iconName
                        tooltipText: model.description
                        highlighted: settingsPage.wide ? ListView.isCurrentItem : false

                        onClicked: {
                            ListView.view.currentIndex = index
                            if (!settingsPage.wide) {
                                filterPane.shown = false
                            }
                        }
                    }
                }

                Keys.onUpPressed: {
                    if (searchMode) {
                        searchQuery.forceActiveFocus()
                    } else {
                        event.accepted = false
                    }
                }
            }
        }

        Rectangle {
            Layout.preferredWidth: units.dp(2)
            Layout.fillHeight: true
            color: theme.palette.normal.foreground
            visible: settingsPage.wide
        }

        ColumnLayout {
            Layout.fillWidth: true
            Layout.fillHeight: true

            Item {
                id: searchBar

                Layout.fillWidth: true
                Layout.topMargin: settingsPage.searchMode ? 0 : -height
                Layout.preferredHeight: units.gu(7)

                Behavior on Layout.topMargin { UbuntuNumberAnimation { duration: UbuntuAnimation.SnapDuration } }
                
                Common.CustomizedTextField {
                    id: searchQuery
                    objectName: "searchQuery"

                    readonly property var terms: text.split(/\s+/g).filter(function(term) { return term.length > 0 })

                    anchors {
                        left: parent.left
                        right: parent.right
                        margins: units.gu(2)
                        verticalCenter: parent.verticalCenter
                    }

                    leftIcon.name: "search"
                    placeholderText: i18n.tr("Search settings")
                    inputMethodHints: Qt.ImhNoPredictiveText

                    onTextChanged: searchDelay.restart()

                    Keys.onDownPressed: mainScrollView.forceActiveFocus()
                    Keys.onEscapePressed: settingsPage.searchMode = false

                    Timer {
                        id: searchDelay
                        interval: 300
                        onTriggered: filteredModel.update(searchQuery.text.trim())
                    }
                }
            }

            Item {
                Layout.fillWidth: true
                Layout.fillHeight: true
                Layout.bottomMargin: units.gu(1)

                ScrollView {
                    id: mainScrollView

                    anchors.fill: parent

                    Common.BaseFlickable {
                        id: mainFlickable

                        anchors.fill: parent
                        contentHeight: contentColumn.height
                        pageHeader: settingsPage.pageManager ? settingsPage.pageManager.header : settingsPage.header
                        ColumnLayout {
                            id: contentColumn

                            anchors {
                                left: parent.left
                                right: parent.right
                            }

                            QQC2.Label {
                                Layout.fillWidth: true
                                Layout.preferredHeight: browser.osk.visible ? mainScrollView.height - browser.osk.height : mainScrollView.height

                                visible: settingsPage.searchIsActive && settingsCol.visibleChildren.length == 1 // 1 = Repeater
                                text: searchDelay.running ? i18n.tr("Searching available settings...") : i18n.tr("No results found")
                                verticalAlignment: Text.AlignVCenter
                                horizontalAlignment: Text.AlignHCenter
                            }

                            ColumnLayout {
                                id: settingsCol

                                Repeater {
                                    model: filteredModel
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    Loader {
        id: bottomGesturesAreaPreview

        readonly property real visibleOpacity: 0.8

        active: opacity > 0
        asynchronous: true
        height: units.gu(settingsPage.settingsObject.bottomGesturesAreaHeight)
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }
        opacity: 0
        visible: opacity > 0
        Behavior on opacity { UbuntuNumberAnimation {} }
        sourceComponent: Rectangle {
            color: theme.palette.normal.activity
        }

        function show() {
            opacity = visibleOpacity
            delayHide.restart()
        }

        function hide() {
            opacity = 0
        }

        Timer {
            id: delayHide
            running: false
            interval: 1000
            onTriggered: bottomGesturesAreaPreview.hide()
        }

        Connections {
            target: settingsPage.settingsObject
            onBottomGesturesAreaHeightChanged: {
                bottomGesturesAreaPreview.height = Qt.binding( function() {return units.gu(settingsPage.settingsObject.bottomGesturesAreaHeight) })
                bottomGesturesAreaPreview.show()
            }
            onBottomGesturesAreaHeightBigUIModeChanged: {
                bottomGesturesAreaPreview.height = Qt.binding( function() {return units.gu(settingsPage.settingsObject.bottomGesturesAreaHeightBigUIMode) })
                bottomGesturesAreaPreview.show()
            }
        }
    }

    Common.SettingsItemFactory {
        id: settingsItemFactory

        settingsObject: settingsPage.settingsObject
    }

    QtObject {
        id: internal

        function showConfirmationDialog(properties) {
            var confirmDialog = confirmationDialogComponent.createObject(settingsPage, properties)
            if (!settingsPage.wide && settingsPage.displayedInFullWidthPanel) {
                confirmDialog.openBottom();
            } else {
                confirmDialog.openNormal();
            }
        }

        function deleteAllHistory(_itemsToDelete) {
            console.log("History to delete " + JSON.stringify(_itemsToDelete))

            if (_itemsToDelete.includes("browsingHistory")) {
                HistoryModel.clearAll()
            }
            for (var w in webbrowserapp.allWindows) {
                if (_itemsToDelete.includes("closedTabs")) {
                    webbrowserapp.allWindows[w].closedTabHistory = []
                    webbrowserapp.allWindows[w].deletedClosedTabHistory = []
                }
                if (_itemsToDelete.includes("closedWindows")) {
                    webbrowserapp.closedWindows = []
                    webbrowserapp.deletedClosedWindows = []
                }
            }
        }

        function resetAppSettings() {
            settingsObject.restoreDefaults();
            settingsObject.resetDomainPermissions();
            settingsObject.resetDomainSettings();
        }
    }

    Common.PopupPageStack {
        id: popupPageStack

        parent: settingsPage.pageManager
        modal: settingsPage.displayedInFullWidthPanel
        closePolicy: !settingsPage.displayedInFullWidthPanel && !settingsPage.wide ? QQC2.Popup.NoAutoClose
                            : QQC2.Popup.CloseOnEscape | QQC2.Popup.CloseOnPressOutside
        enableShortcuts: settingsPage.enableShortcuts
        onAboutToShow: headerExpanded = settingsPage.pageManager.headerExpanded
    }

    Component {
        id: domainPermissionsComponent

        Common.DomainPermissionsPage {}
    }

    Component {
        id: domainSettingsComponent

        Common.DomainSettingsPage {}
    }

    Component {
        id: leftQuickActionsComponent

        Common.QuickActionsSettingsPage {
            title: i18n.tr("Quick actions (Left)")
            model: settingsPage.settingsObject.webviewQuickActions[0]
            onModelDataChanged: {
                settingsPage.settingsObject.webviewQuickActions = [ newModelData, settingsPage.settingsObject.webviewQuickActions[1] ]
            }
        }
    }

    Component {
        id: rightQuickActionsComponent

        Common.QuickActionsSettingsPage {
            title: i18n.tr("Quick actions (Right)")
            model: settingsPage.settingsObject.webviewQuickActions[1]
            onModelDataChanged: {
                settingsPage.settingsObject.webviewQuickActions = [ settingsPage.settingsObject.webviewQuickActions[0], newModelData ]
            }
        }
    }

    Component {
        id: searchSuggestionsComponent

        Common.SearchSuggestionsSettingsPage {
            model: settingsPage.settingsObject.searchSuggestions
            onModelDataChanged: settingsPage.settingsObject.searchSuggestions = newModelData
        }
    }

    Component {
        id: confirmationDialogComponent

        Common.ConfirmationDialog {
            id: confirmationDialog
        }
    }

    property list<Common.SettingsItem> settingsMainGroupsModel: [
        Common.SettingsItem {
            mainGroup: "all"
            title: i18n.tr("All settings")
            description: i18n.tr("Shows all settings")
            iconName: "stock_website"
        }
        , Common.SettingsItem {
            mainGroup: "general"
            title: i18n.tr("General")
            description: i18n.tr("General settings such as appearance, layout, and browsing settings")
            iconName: "settings"
        }
        ,Common.SettingsItem {
            mainGroup: "gestures"
            title: i18n.tr("Gestures")
            description: i18n.tr("Settings for gestures that are helpful on touch devices")
            iconName: "gestures"
        }
        ,Common.SettingsItem {
            mainGroup: "mobile"
            title: i18n.tr("Mobile")
            description: i18n.tr("Settings that are only applicable to touch and/or mobile use")
            iconName: "phone-smartphone-symbolic"
        }
        ,Common.SettingsItem {
            mainGroup: "search"
            title: i18n.tr("Search")
            description: i18n.tr("Settings related to Search functions")
            iconName: "find"
        }
        ,Common.SettingsItem {
            mainGroup: "privacy"
            title: i18n.tr("Privacy and Security")
            description: i18n.tr("Settings related to privacy and permissions such as whitelists and blacklists")
            iconName: "preferences-system-privacy-symbolic"
        }
        ,Common.SettingsItem {
            mainGroup: "reset"
            title: i18n.tr("Reset and Clear")
            description: i18n.tr("Reset and clear data such as browser settings and browser history")
            iconName: "reset"
        }
    ]

    property list<Common.SettingsItem> settingsItemsModel: [
        Common.SettingsItem {
            mainGroup: "general"
            subGroup: "startup"
            title: i18n.tr("Startup")
            itemType: Common.SettingsItem.ItemType.Header
        }
        , Common.SettingsItem {
            mainGroup: "general"
            subGroup: "startup"
            enabled: !settingsObject.incognitoOnStart
            title: i18n.tr("Restore previous session at startup")
            description: i18n.tr("Restore all windows and tabs from previous session upon opening the browser")
            itemType: Common.SettingsItem.ItemType.CheckBox
            bindProperty: "restoreSession"
        }
        , Common.SettingsItem {
            mainGroup: "general"
            subGroup: "startup"
            title: i18n.tr("Start in private mode")
            description: i18n.tr("Starts the browser in Private Mode")
            itemType: Common.SettingsItem.ItemType.CheckBox
            bindProperty: "incognitoOnStart"
        }
        , Common.SettingsItem {
            mainGroup: "general"
            subGroup: "home"
            title: i18n.tr("Home")
            itemType: Common.SettingsItem.ItemType.Header
        }
        , Common.SettingsItem {
            readonly property url defaultHomePage: "https://start.duckduckgo.com"

            mainGroup: "general"
            subGroup: "home"
            title: i18n.tr("Homepage")
            description: i18n.tr("Determines the home page for the browser")
            itemType: Common.SettingsItem.ItemType.GroupedRadio
            itemModel: [
                { "value": "", "text": i18n.tr("New tab page") }
                , { "value": defaultHomePage, "text": "start.duckduckgo.com" }
                , { "value": "custom", "text": i18n.tr("Custom hompage")
                    , "customFieldType": Common.GroupedRadioButtons.CustomFieldType.TextField
                    , "customInputMethodHints": Qt.ImhNoAutoUppercase | Qt.ImhNoPredictiveText | Qt.ImhUrlCharactersOnly
                  }
            ]
            itemResetValue: ""
            itemCustomValue: settingsObject.homepage
            itemSettingsToControl: function(value) {
                if (value.toString() === "") {
                    return 0
                } else if (value == defaultHomePage) {
                    return 1
                } else {
                    return 2
                }
            }
            itemControlToSettings: function(value) {
                switch (value) {
                    case 0:
                        return ""
                    case 1:
                        return defaultHomePage
                    case 2:
                        return itemCustomValue
                }
            }
            itemCustomValueValidator: function(value) {
                return value ? UrlUtils.looksLikeAUrl(value.toString().trim()) : false
            }
            itemCustomValueToSettings: function(value) {
                return value ? UrlUtils.fixUrl(value.toString()) : false
            }
            bindProperty: "homepage"
        }
        ,Common.SettingsItem {
            mainGroup: "general"
            subGroup: "appearance"
            title: i18n.tr("Appearance and Layout")
            itemType: Common.SettingsItem.ItemType.Header
        }
        , Common.SettingsItem {
            mainGroup: "general"
            subGroup: "appearance"
            visible: browser.wide
            title: i18n.tr("Tabs bar")
            description: TextUtils.bulletTextArray(
            [
                i18n.tr("Displays a traditional tabs bar at the topmost part of the browser")
                ,i18n.tr("Only appears when the browser is wide enough")
            ])
            itemType: Common.SettingsItem.ItemType.Switch
            bindProperty: "hideTabsBar"
            invertedBind: true
        }
        , Common.SettingsItem {
            mainGroup: "general"
            subGroup: "appearance"
            title: i18n.tr("Tabs button in the navigation bar")
            description: i18n.tr("Displays a button in the navigation bar for opening the tabs list view ")
            itemType: Common.SettingsItem.ItemType.Switch
            bindProperty: "hideTabsButtonNavBar"
            invertedBind: true
        }
        , Common.SettingsItem {
            mainGroup: "general"
            subGroup: "appearance"
            title: i18n.tr("Navigation bar hide behavior")
            description: i18n.tr("Determines the behavior of the Navigation bar for automatically hiding.")
                + " "
                + i18n.tr("This does not apply when the app is large and tall enough because the Navigation bar will always be shown in that state.")
                + "\n"
                + TextUtils.bulletTextArray(
                    [
                        i18n.tr("Fixed: Always shown and never hides")
                        ,i18n.tr("On scroll: Hides when scrolling down and shows when scrolling up")
                        ,i18n.tr("Times out: Hides after a predefined time and shows when bottom side gestures are used as well as when hovering at the topmost part of the app.")
                        ,i18n.tr("Always hide: Hides after a predefined time and shows when hovering at the topmost part of the app.")
                    ])
            itemType: Common.SettingsItem.ItemType.ComboBox
            bindProperty: "headerHide"
            itemModel: [
                { "value": 0, "text": i18n.tr("Fixed") }
                , { "value": 1, "text": i18n.tr("On scroll") }
                , { "value": 2, "text": i18n.tr("Times out") }
                , { "value": 3, "text": i18n.tr("Always hide") }
            ]
            itemMaximumWidth: settingsPage.wide ? units.gu(20) : 0
        }
        , Common.SettingsItem {
            mainGroup: "general"
            subGroup: "zoom"
            title: i18n.tr("Zoom")
            itemType: Common.SettingsItem.ItemType.Header
        }
        , Common.SettingsItem {
            mainGroup: "general"
            subGroup: "zoom"
            title: i18n.tr("Default zoom")
            description: TextUtils.bulletTextArray(
            [
                i18n.tr("Adjusts the default zoom when displaying sites and applies browser-wide")
                ,i18n.tr("This can be overriden by setting the zoom from the menu or via keyboard shortcuts which control zoom defaults per domain")
            ])
            itemType: Common.SettingsItem.ItemType.SpinBox
            bindProperty: "zoomFactor"
            itemMaximumWidth: settingsPage.wide ? units.gu(20) : 0

            // SpinBox properties
            itemResetValue: 1.0
            itemFromValue: 25
            itemToValue: 500
            itemStepSizeValue: 5
            itemTextFromValue: function(value, locale) {
                return value + "%";
            }
            itemSettingsToControl: function(value) {
                return Math.round(value * 100 * itemStepSizeValue) / itemStepSizeValue
            }
            itemControlToSettings: function(value) {
                return (Math.round(value / itemStepSizeValue) * itemStepSizeValue) / 100
            }
        }
        , Common.SettingsItem {
            mainGroup: "general"
            subGroup: "zoom"
            title: i18n.tr("Automatically fit the web page to the web view's width")
            description: TextUtils.bulletTextArray(
            [
                i18n.tr("Adjusts the width of the website to the web view effectively zooming it out")
                ,i18n.tr("This can be helpful in desktop sites or sites without proper mobile layout support")
            ])
            itemType: Common.SettingsItem.ItemType.CheckBox
            bindProperty: "autoFitToWidthEnabled"
        }
        , Common.SettingsItem {
            mainGroup: "general"
            subGroup: "browsing"
            title: i18n.tr("Browsing")
            itemType: Common.SettingsItem.ItemType.Header
        }
        , Common.SettingsItem {
            mainGroup: "general"
            subGroup: "browsing"
            title: i18n.tr("Automatically load images on web pages")
            description: i18n.tr("When disabled, images won't be automatically downloaded and displayed in web pages")
            itemType: Common.SettingsItem.ItemType.Switch
            bindProperty: "loadImages"
        }
        , Common.SettingsItem {
            mainGroup: "general"
            subGroup: "browsing"
            title: i18n.tr("Switch to existing tab when trying to open a link to new tab")
            description: TextUtils.bulletTextArray(
            [
                i18n.tr("When trying to open a link to new tab, you will be switched to an existing tab if the URL is exactly the same.")
                ,i18n.tr("When trying to open in a background tab, instead of immediately switching to the tab, it will be moved next to the current tab in the tabs bar and the tabs switcher")
            ])
            itemType: Common.SettingsItem.ItemType.Switch
            bindProperty: "openLinkInExistingTab"
        }
        , Common.SettingsItem {
            mainGroup: "general"
            subGroup: "browsing"
            title: i18n.tr("Scroll to top/bottom button")
            description: TextUtils.bulletTextArray(
            [
                i18n.tr("Displays a floating button at the bottom right when scrolling a web page.")
                ,i18n.tr("It can scroll the web page to the top or the bottom.")
                ,i18n.tr("The arrow indicates the scroll direction when you click it.")
            ])
            itemType: Common.SettingsItem.ItemType.Switch
            bindProperty: "appWideScrollPositioner"
        }
        , Common.SettingsItem {
            mainGroup: "general"
            subGroup: "browsing"
            visible: browser.currentWebview && browser.currentWebview.context.__ua.calcScreenSize() == "small"
            title: i18n.tr("Request desktop version of sites as default")
            description: TextUtils.bulletTextArray(
            [
                i18n.tr("The browser will request the desktop version of sites unless overridden by the tab.")
                ,i18n.tr("This is achieved by changing the user agent presented to sites.")
            ])
            itemType: Common.SettingsItem.ItemType.CheckBox
            bindProperty: "setDesktopMode"
        }
        , Common.SettingsItem {
            mainGroup: "general"
            subGroup: "browsing"
            visible: browser.currentWebview && browser.currentWebview.context.__ua.calcScreenSize() !== "small"
            title: i18n.tr("Request mobile version of sites as default")
            description: TextUtils.bulletTextArray(
            [
                i18n.tr("The browser will request the mobile version of sites unless overridden by the tab.")
                ,i18n.tr("This is achieved by changing the user agent presented to sites.")
            ])
            itemType: Common.SettingsItem.ItemType.CheckBox
            bindProperty: "forceMobileSite"
        }
        , Common.SettingsItem {
            mainGroup: "general"
            subGroup: "browsing"
            title: i18n.tr("Automatically switch between Desktop and Mobile version of sites based on web view's width")
            description: TextUtils.bulletTextArray(
            [
                i18n.tr("The requested site version will automatically switch between Desktop and Mobile when the web view's width changes.")
                ,i18n.tr("When the web view is wide enough, the browser will present itself as a desktop device.")
                ,i18n.tr("When the web view is narrow enough, the browser will present itself as a mobile device.")
                ,i18n.tr("This is achieved by changing the user agent presented to sites.")
            ])
            itemType: Common.SettingsItem.ItemType.CheckBox
            bindProperty: "autoDeskMobSwitch"
        }
        , Common.SettingsItem {
            mainGroup: "general"
            subGroup: "browsing"
            title: i18n.tr("Automatically reload the page when requested site version is changed")
            description: i18n.tr("The page is reloaded automatically when the requested site version was changed")
            itemType: Common.SettingsItem.ItemType.CheckBox
            bindProperty: "autoDeskMobSwitchReload"
        }
        , Common.SettingsItem {
            mainGroup: "general"
            subGroup: "media"
            visible: browser.thisWindow.sensorExists
            title: i18n.tr("Media")
            itemType: Common.SettingsItem.ItemType.Header
        }
        , Common.SettingsItem {
            mainGroup: "general"
            subGroup: "media"
            visible: browser.thisWindow.sensorExists
            title: i18n.tr("Automatically rotate videos on full screen")
            description: i18n.tr("Rotate videos when entering full screen based on their aspect ratio and current device orientation")
            itemType: Common.SettingsItem.ItemType.CheckBox
            bindProperty: "autoVideoRotate"
        }
        , Common.SettingsItem {
            mainGroup: "gestures"
            subGroup: "options"
            title: i18n.tr("Gestures Options")
            itemType: Common.SettingsItem.ItemType.Header
        }
        , Common.SettingsItem {
            mainGroup: "gestures"
            subGroup: "options"
            title: i18n.tr("Physical unit-based gestures")
            description: TextUtils.bulletTextArray(
            [
                i18n.tr("Dimension and threshold of most gesture areas will be in a physical unit (inch)")
                ,i18n.tr("This makes gestures consistent across different screen sizes, scaling, aspect ratio and orientation")
                ,i18n.tr("When disabled, gesture behavior may vary depending on screen size, scaling, etc")
            ])
            itemType: Common.SettingsItem.ItemType.Switch
            bindProperty: "physicalForGestures"
        }
        , Common.SettingsItem {
            mainGroup: "gestures"
            subGroup: "options"
            title: i18n.tr("Haptic feedback")
            description: i18n.tr("Play haptic feedback when using gestures and pressing buttons that perform actions.")
            itemType: Common.SettingsItem.ItemType.Switch
            bindProperty: "enableHaptics"
        }
        , Common.SettingsItem {
            mainGroup: "gestures"
            subGroup: "options"
            title: i18n.tr("Bottom gesture area height")
            description: i18n.tr("The height of the gesture area at the bottom of the app that detects vertical or horizontal swipes")
            itemType: Common.SettingsItem.ItemType.Slider
            bindProperty: "bottomGesturesAreaHeight"
            itemMaximumWidth: settingsPage.wide ? units.gu(60) : 0

            // Slider properties
            itemResetValue: 2
            itemFromValue: 1
            itemToValue: 6
            itemStepSizeValue: 0.25
            itemLiveValue: true
            itemDisplayCurrentValue: false
            itemDisplayCurrentValueOnControl: false
            itemRoundDisplayedValue: true
            itemRoundingDecimal: 2
            itemEnableFineControls: true
            itemSettingsToControl: function(value) {
                return value;
            }
            itemControlToSettings: function(value) {
                return parseFloat(value.toFixed(2))
            }
        }
        , Common.SettingsItem {
            mainGroup: "gestures"
            subGroup: "webview"
            title: i18n.tr("Web View Gestures")
            itemType: Common.SettingsItem.ItemType.Header
        }
        , Common.SettingsItem {
            mainGroup: "gestures"
            subGroup: "webview"
            title: i18n.tr("Directly search tabs")
            description: i18n.tr("Directly search tabs by swiping up and holding when opening the tabs list")
            itemType: Common.SettingsItem.ItemType.CheckBox
            bindProperty: "webviewBottomHoldTabsSearch"
        }
        , Common.SettingsItem {
            mainGroup: "gestures"
            subGroup: "webview"
            title: i18n.tr("Bottom gesture hint")
            description: i18n.tr("Displays a visual hint for the bottom gestures")
            + "\n"
            + TextUtils.bulletTextArray(
            [
                i18n.tr("The hint indicates the space where you can swipe up to open the tabs list and where you can swipe horizontally to go back/forward in the web view")
                ,i18n.tr("The empty space on the left and right side indicate where you can swipe up to access shortcuts")
            ])
            itemType: Common.SettingsItem.ItemType.Switch
            bindProperty: "hideBottomHint"
            invertedBind: true
        }
        , Common.SettingsItem {
            mainGroup: "gestures"
            subGroup: "webview"
            title: i18n.tr("Horizontal swipe gesture")
            description: i18n.tr("Enables a gesture area around the middle part of the bottom edge")
            + "\n"
            + TextUtils.bulletTextArray(
            [
                i18n.tr("Swipe to the right to go back one page and to the left to go forward")
                ,i18n.tr("Swipe to either direction and hold to open the corresponding navigation history")
            ])
            itemType: Common.SettingsItem.ItemType.Switch
            bindProperty: "webviewHorizontalSwipe"
        }
        , Common.SettingsItem {
            mainGroup: "gestures"
            subGroup: "webview"
            title: i18n.tr("Bottom side gesture")
            description: i18n.tr("Enables gesture areas at the leftmost and rightmost parts of the bottom edge")
            + "\n"
            + TextUtils.bulletTextArray(
            [
                i18n.tr("Left swipe opens a horizontal bottom menu with actions such as back, forward, and reload")
                ,i18n.tr("Right swipe opens a horizontal bottom menu with actions such as open tabs list and open menu or directly open the menu")
                ,i18n.tr("Available actions depends on the actions present on the left or right side of the navigation bar")
                ,i18n.tr("When there's only one action is available, it will be triggered immediately upon performing the swipe")
                ,i18n.tr("When Quick actions is enabled, Quick swipe has to be enabled to retain the original function")
            ])
            itemType: Common.SettingsItem.ItemType.Switch
            bindProperty: "webviewSideSwipe"
        }
        , Common.SettingsItem {
            mainGroup: "gestures"
            subGroup: "webview"
            title: i18n.tr("Quick actions")
            itemLevel: 1
            enabled: settingsPage.settingsObject.webviewSideSwipe
            description: i18n.tr("Enables a vertical menu for quickly performing actions from either side of the bottom edge")
            + "\n"
            + TextUtils.bulletTextArray(
            [
                i18n.tr("Swiping up from the left/right side without lifting your finger will reveal a menu of configurable list of actions")
                ,i18n.tr("Lifting your swipe will perform the currently selected action")
                ,i18n.tr("You can cancel the gesture without triggering an action by swiping down to the bottom again and release from there")
            ])
            itemType: Common.SettingsItem.ItemType.Switch
            bindProperty: "webviewEnableQuickActions"
        }
        , Common.SettingsItem {
            mainGroup: "gestures"
            subGroup: "webview"
            title: i18n.tr("Max height")
            itemLevel: 2
            enabled: settingsPage.settingsObject.webviewEnableQuickActions
            description: i18n.tr("Maximum height of the Quick actions menu when there's enough available height")
            itemType: Common.SettingsItem.ItemType.Slider
            bindProperty: "webviewQuickActionsHeight"
            itemMaximumWidth: settingsPage.wide ? units.gu(60) : 0

            // Slider properties
            itemResetValue: 3
            itemFromValue: 1
            itemToValue: 5
            itemStepSizeValue: 0.25
            itemLiveValue: false
            itemDisplayCurrentValue: true
            itemDisplayCurrentValueOnControl: true
            itemRoundDisplayedValue: true
            itemRoundingDecimal: 2
            itemEnableFineControls: true
            itemUnit: i18n.tr("inch")
            itemSettingsToControl: function(value) {
                return value;
            }
            itemControlToSettings: function(value) {
                return parseFloat(value.toFixed(2))
            }
        }
        , Common.SettingsItem {
            mainGroup: "gestures"
            subGroup: "webview"
            title: i18n.tr("Quick swipe")
            itemLevel: 2
            enabled: settingsPage.settingsObject.webviewSideSwipe && settingsPage.settingsObject.webviewEnableQuickActions
            description: TextUtils.bulletTextArray(
            [
                i18n.tr("Enable this to retain the original function of the bottom side gesture by doing a quick swipe")
                ,i18n.tr("A quick swipe means swiping up with enough distance and lifting the finger immediately before triggering the Quick actions menu")
            ])
            itemType: Common.SettingsItem.ItemType.Switch
            bindProperty: "webviewQuickSideSwipe"
        }
        , Common.SettingsItem {
            mainGroup: "gestures"
            subGroup: "webview"
            title: i18n.tr("Quick actions delay")
            itemLevel: 2
            enabled: settingsPage.settingsObject.webviewSideSwipe && settingsPage.settingsObject.webviewEnableQuickActions
                                && !settingsPage.settingsObject.webviewQuickSideSwipe
            description: TextUtils.bulletTextArray(
            [
                i18n.tr("Enables a delay before the Quick action menu will appear when swiping")
                ,i18n.tr("Disabled state only takes effect when Quick swipe is disabled")
            ])
            itemType: Common.SettingsItem.ItemType.Switch
            bindProperty: "webviewQuickActionEnableDelay"
        }
        , Common.SettingsItem {
            mainGroup: "gestures"
            subGroup: "webview"
            title: i18n.tr("Quick actions (Left)")
            itemLevel: 2
            enabled: settingsPage.settingsObject.webviewSideSwipe && settingsPage.settingsObject.webviewEnableQuickActions
            description: i18n.tr("Add, remove and rearrange the quick actions available from the left bottom gesture in the web view")
            itemType: Common.SettingsItem.ItemType.Action
            itemFunction: function() {
                popupPageStack.openWithItem(leftQuickActionsComponent)
            }
        }
        , Common.SettingsItem {
            mainGroup: "gestures"
            subGroup: "webview"
            title: i18n.tr("Quick actions (Right)")
            itemLevel: 2
            enabled: settingsPage.settingsObject.webviewSideSwipe && settingsPage.settingsObject.webviewEnableQuickActions
            description: i18n.tr("Add, remove and rearrange the quick actions available from the right bottom gesture in the web view")
            itemType: Common.SettingsItem.ItemType.Action
            itemFunction: function() {
                popupPageStack.openWithItem(rightQuickActionsComponent)
            }
        }
        , Common.SettingsItem {
            mainGroup: "gestures"
            subGroup: "webview"
            title: i18n.tr("Web view pull down")
            description: i18n.tr("Enables gesture area at the leftmost and rightmost side of the web view for pulling down/up the web view.")
                + " "
                + i18n.tr("This helps ease reaching top elements in a web page.")
                + "\n"
                + TextUtils.bulletTextArray(
                    [
                        i18n.tr("Swipe down and release: Pulls down the web view to near half of the app")
                        ,i18n.tr("Swipe up and release: Resets back the web view to its normal state")
                    ])
            itemType: Common.SettingsItem.ItemType.Switch
            bindProperty: "enableWebviewPullDownGestures"
        }
        , Common.SettingsItem {
            mainGroup: "gestures"
            subGroup: "apppages"
            title: i18n.tr("App Pages Gestures")
            itemType: Common.SettingsItem.ItemType.Header
        }
        , Common.SettingsItem {
            mainGroup: "gestures"
            subGroup: "apppages"
            title: i18n.tr("Horizontal swipe gesture")
            description: i18n.tr("Enables a gesture area around the middle part of the bottom edge")
            + "\n"
            + TextUtils.bulletTextArray(
            [
                i18n.tr("Swipe to either direction to open a bottom menu with the corresponding actions from the page header")
                ,i18n.tr("When there's only one action is available, it will be triggered immediately upon performing the swipe")
            ])
            itemType: Common.SettingsItem.ItemType.Switch
            bindProperty: "appPagesHorizontalSwipe"
        }
        , Common.SettingsItem {
            mainGroup: "gestures"
            subGroup: "apppages"
            title: i18n.tr("Gesture hint")
            itemLevel: 1
            enabled: settingsPage.settingsObject.appPagesHorizontalSwipe
            description: i18n.tr("Displays a visual hint for the Horizontal swipe gesture")
            + "\n"
            + TextUtils.bulletTextArray(
            [
                i18n.tr("The hint indicates the space where you can swipe horizontally to trigger page header actions")
                ,i18n.tr("When Bottom side gesture is enabled, the empty space on the left and right side indicate where you can swipe up to access its functions")
            ])
            itemType: Common.SettingsItem.ItemType.Switch
            bindProperty: "appPagesHorizontalSwipeHint"
        }
        , Common.SettingsItem {
            mainGroup: "gestures"
            subGroup: "apppages"
            title: i18n.tr("Bottom side gesture")
            description: i18n.tr("Enables gesture areas at the leftmost and rightmost parts of the bottom edge")
            + "\n"
            + TextUtils.bulletTextArray(
            [
                i18n.tr("A swipe will open a menu with all the actions from the corresponding side of the page header")
                ,i18n.tr("When there's only one action is available, it will be triggered immediately upon performing the swipe")
                ,i18n.tr("When Direct actions is enabled, Quick swipe has to be enabled to retain the original function")
            ])
            itemType: Common.SettingsItem.ItemType.Switch
            bindProperty: "appPagesSideSwipe"
        }
        , Common.SettingsItem {
            mainGroup: "gestures"
            subGroup: "apppages"
            title: i18n.tr("Direct actions")
            itemLevel: 1
            enabled: settingsPage.settingsObject.appPagesSideSwipe
            description: i18n.tr("Enables a vertical menu for quickly performing actions that are present in the page header.")
            + " "
            + i18n.tr("The list of actions correspond to the same actions present in the page header.")
            + "\n"
            + TextUtils.bulletTextArray(
            [
                i18n.tr("Swiping up from the left/right side without lifting your finger will reveal a menu of list of actions")
                ,i18n.tr("Lifting your swipe will perform the currently selected action")
                ,i18n.tr("You can cancel the gesture without triggering an action by swiping down to the bottom again and release from there")
            ])
            itemType: Common.SettingsItem.ItemType.Switch
            bindProperty: "appPagesDirectActions"
        }
        , Common.SettingsItem {
            mainGroup: "gestures"
            subGroup: "apppages"
            title: i18n.tr("Max height")
            itemLevel: 2
            enabled: settingsPage.settingsObject.appPagesDirectActions
            description: i18n.tr("Maximum height of the Direct actions menu when there's enough available height")
            itemType: Common.SettingsItem.ItemType.Slider
            bindProperty: "appPagesQuickActionsHeight"
            itemMaximumWidth: settingsPage.wide ? units.gu(60) : 0

            // Slider properties
            itemResetValue: 3
            itemFromValue: 1
            itemToValue: 5
            itemStepSizeValue: 0.25
            itemLiveValue: false
            itemDisplayCurrentValue: true
            itemDisplayCurrentValueOnControl: true
            itemRoundDisplayedValue: true
            itemRoundingDecimal: 2
            itemEnableFineControls: true
            itemUnit: i18n.tr("inch")
            itemSettingsToControl: function(value) {
                return value;
            }
            itemControlToSettings: function(value) {
                return parseFloat(value.toFixed(2))
            }
        }
        , Common.SettingsItem {
            mainGroup: "gestures"
            subGroup: "apppages"
            title: i18n.tr("Quick swipe")
            itemLevel: 2
            enabled: settingsPage.settingsObject.appPagesSideSwipe && settingsPage.settingsObject.appPagesDirectActions
            description: TextUtils.bulletTextArray(
            [
                i18n.tr("Enable this to retain the original function of the bottom side gesture by doing a quick swipe")
                ,i18n.tr("A quick swipe means swiping up with enough distance and lifting the finger immediately before triggering the Quick actions menu")
            ])
            itemType: Common.SettingsItem.ItemType.Switch
            bindProperty: "appPagesQuickSideSwipe"
        }
        , Common.SettingsItem {
            mainGroup: "gestures"
            subGroup: "apppages"
            title: i18n.tr("Direct actions delay")
            itemLevel: 2
            enabled: settingsPage.settingsObject.appPagesSideSwipe && settingsPage.settingsObject.appPagesDirectActions
                                && !settingsPage.settingsObject.appPagesQuickSideSwipe
            description: TextUtils.bulletTextArray(
            [
                i18n.tr("Enables a delay before the Direct action menu will appear when swiping")
                ,i18n.tr("Disabled state only takes effect when Quick swipe is disabled")
            ])
            itemType: Common.SettingsItem.ItemType.Switch
            bindProperty: "appPagesQuickActionEnableDelay"
        }
        , Common.SettingsItem {
            mainGroup: "gestures"
            subGroup: "apppages"
            title: i18n.tr("Header pull down")
            description: i18n.tr("Dragging the page contents down will expand the header's height to near half of the page")
                + " "
                + i18n.tr("This helps ease reaching top elements in the page.")
                + "\n"
                + TextUtils.bulletTextArray(
                    [
                        i18n.tr("Swipe down and release: Expands the header's height to near half of the app")
                        ,i18n.tr("Swipe up and release: Resets back the header to its normal state")
                    ])
            itemType: Common.SettingsItem.ItemType.Switch
            bindProperty: "appPagesPullDown"
        }
        , Common.SettingsItem {
            mainGroup: "mobile"
            subGroup: "features"
            title: i18n.tr("Mobile Features")
            itemType: Common.SettingsItem.ItemType.Header
        }
        , Common.SettingsItem {
            mainGroup: "mobile"
            subGroup: "features"
            title: i18n.tr("Enable EasyTarget mode toggle")
            description: TextUtils.bulletTextArray(
            [
                i18n.tr("This adds a toggle in the drawer menu for enabling/disabling EasyTarget mode")
                ,i18n.tr("This mode makes the app easier to use in certain situations such as when mounted to a phone holder and driving....in a traffic :D")
                ,i18n.tr("This is done by making the gesture areas and some UI elements bigger")
            ])
            itemType: Common.SettingsItem.ItemType.Switch
            bindProperty: "enableBigUIModeToggle"
        }
        , Common.SettingsItem {
            mainGroup: "mobile"
            subGroup: "features"
            enabled: settingsPage.settingsObject.enableBigUIModeToggle
            title: i18n.tr("Bottom gesture area height")
            description: i18n.tr("The height of the gesture area at the bottom of the app that detects vertical or horizontal swipes. This will also be the bottom margin of the contents.")
            itemType: Common.SettingsItem.ItemType.Slider
            bindProperty: "bottomGesturesAreaHeightBigUIMode"
            itemMaximumWidth: settingsPage.wide ? units.gu(60) : 0
            itemLevel: 1

            // Slider properties
            itemResetValue: 4
            itemFromValue: 2
            itemToValue: 15
            itemStepSizeValue: 0.25
            itemLiveValue: true
            itemDisplayCurrentValue: false
            itemDisplayCurrentValueOnControl: false
            itemRoundDisplayedValue: true
            itemRoundingDecimal: 2
            itemEnableFineControls: true
            itemSettingsToControl: function(value) {
                return value;
            }
            itemControlToSettings: function(value) {
                return parseFloat(value.toFixed(2))
            }
        }
        , Common.SettingsItem {
            mainGroup: "mobile"
            subGroup: "features"
            title: i18n.tr("Bottom gesture hint")
            itemLevel: 1
            enabled: settingsPage.settingsObject.enableBigUIModeToggle
            description: i18n.tr("Displays a visual hint for the bottom gestures area")
            + "\n"
            + TextUtils.bulletTextArray(
            [
                i18n.tr("The hint indicates the space where you can swipe up and/or swipe horizontally")
                ,i18n.tr("The empty space on the left and right side indicate where you can swipe up to access shortcuts")
            ])
            itemType: Common.SettingsItem.ItemType.Switch
            bindProperty: "bigUIModeBottomHint"
        }
        , Common.SettingsItem {
            mainGroup: "mobile"
            subGroup: "features"
            title: i18n.tr("Navigation bar hide behavior")
            enabled: settingsPage.settingsObject.enableBigUIModeToggle
            itemLevel: 1
            description: i18n.tr("Determines the behavior of the Navigation bar for automatically hiding.")
                + " "
                + i18n.tr("This does not apply when the app is large and tall enough because the Navigation bar will always be shown in that state.")
                + "\n"
                + TextUtils.bulletTextArray(
                    [
                        i18n.tr("Fixed: Always shown and never hides")
                        ,i18n.tr("On scroll: Hides when scrolling down and shows when scrolling up")
                        ,i18n.tr("Times out: Hides after a predefined time and shows when bottom side gestures are used as well as when hovering at the topmost part of the app.")
                        ,i18n.tr("Always hide: Hides after a predefined time and shows when hovering at the topmost part of the app.")
                    ])
            itemType: Common.SettingsItem.ItemType.ComboBox
            bindProperty: "bigUIModeHeaderHide"
            itemModel: [
                { "value": 0, "text": i18n.tr("Fixed") }
                , { "value": 1, "text": i18n.tr("On scroll") }
                , { "value": 2, "text": i18n.tr("Times out") }
                , { "value": 3, "text": i18n.tr("Always hide") }
            ]
            itemMaximumWidth: settingsPage.wide ? units.gu(20) : 0
        }
        , Common.SettingsItem {
            mainGroup: "search"
            subGroup: "searchengine"
            title: i18n.tr("Search Engine")
            itemType: Common.SettingsItem.ItemType.Header
        }
        , Common.SettingsItem {
            mainGroup: "search"
            subGroup: "searchengine"
            visible: searchEngines.engines.count > 1
            title: i18n.tr("Default search engine")
            description: i18n.tr("Determines which search engine will be used when searching from the address bar.")
            itemType: Common.SettingsItem.ItemType.ComboBox
            bindProperty: "searchEngine"
            itemModel: searchEngines.engines
            itemModelTextRole: "name"
            itemModelValueRole: "filename"
            itemMaximumWidth: settingsPage.wide ? units.gu(30) : 0
        }
        , Common.SettingsItem {
            mainGroup: "search"
            subGroup: "addressbar"
            title: i18n.tr("Address Bar")
            itemType: Common.SettingsItem.ItemType.Header
        }
        , Common.SettingsItem {
            mainGroup: "search"
            subGroup: "addressbar"
            title: i18n.tr("Search suggestions")
            description: TextUtils.bulletTextArray(
            [
                i18n.tr("Enable or disable different types of suggestions when using the address bar")
                ,i18n.tr("They can also be rearranged and set various options such as result number limit")
            ])
            itemType: Common.SettingsItem.ItemType.Action
            itemFunction: function() {
                popupPageStack.openWithItem(searchSuggestionsComponent)
            }
        }
        , Common.SettingsItem {
            mainGroup: "search"
            subGroup: "addressbar"
            title: i18n.tr("Enable Focused search")
            description: i18n.tr("Focused search filters the suggestions based on the selected type hinted by the indicator.")
                + " "
                + i18n.tr("This is achieved by adding specific numbers of leading or trailing spaces to the search text.")
                + " "
                + i18n.tr('Pressing "Enter" will automatically select the first in the results list.')
                + "\n"
                + TextUtils.bulletTextArray(
                    [
                        i18n.tr("1 leading / 2 trailing: Bookmarks")
                        ,i18n.tr("2 leading / 3 trailing: History")
                        ,i18n.tr("3 leading / 4 trailing: Tabs")
                    ])
            itemType: Common.SettingsItem.ItemType.CheckBox
            bindProperty: "focusedSearch"
        }
        , Common.SettingsItem {
            mainGroup: "search"
            subGroup: "tabssearch"
            title: i18n.tr("Tabs Search")
            itemType: Common.SettingsItem.ItemType.Header
        }
        , Common.SettingsItem {
            mainGroup: "search"
            subGroup: "tabssearch"
            title: i18n.tr("Enable immediate search by adding a trailing space")
            description: i18n.tr("Enabling this will bypass the delay when searching tabs by adding a trailing space to the search text.")
                + " "
                + i18n.tr("There is a very brief delay in reflecting the results from the search text.")
                + " "
                + i18n.tr('This can be helpful when searching for a very specific tab that you are sure exists so that you can immediately press "Enter" to select that tab.')
            itemType: Common.SettingsItem.ItemType.CheckBox
            bindProperty: "tabsSearchBypassDelay"
        }
        , Common.SettingsItem {
            mainGroup: "search"
            subGroup: "tabssearch"
            title: i18n.tr("Enable new tab suggestions")
            description: i18n.tr("Enabling this will add options to open a new tab to either search or go to the URL using the search text")
                + "\n"
                + TextUtils.bulletTextArray(
                    [
                        i18n.tr("The options will appear when the search text returns no result")
                        ,i18n.tr("The options will also appear by adding 2 trailing spaces to the search text")
                    ])
            itemType: Common.SettingsItem.ItemType.CheckBox
            bindProperty: "tabsSearchNewTabSuggestions"
        }
        , Common.SettingsItem {
            mainGroup: "privacy"
            subGroup: "permissions"
            title: i18n.tr("Permissions")
            itemType: Common.SettingsItem.ItemType.Header
        }
        , Common.SettingsItem {
            mainGroup: "privacy"
            subGroup: "permissions"
            title: i18n.tr("Only allow whitelisted sites")
            description: i18n.tr("All sites will be rejected by default and only whitelisted sites will be allowed.")
            itemType: Common.SettingsItem.ItemType.CheckBox
            bindProperty: "domainWhiteListMode"
        }
        , Common.SettingsItem {
            mainGroup: "privacy"
            subGroup: "permissions"
            title: i18n.tr("Ask when site tries to open a new view")
            description: TextUtils.bulletTextArray(
            [
                i18n.tr("You will be asked whether to allow or deny whenever the current site is trying to open a new tab/window")
                ,i18n.tr("An option to remember the decision for the current domain or for all sites is offered")
                ,i18n.tr("Remembering the decision for the current domain will add or edit an entry in the Domain-specific settings page")
                ,i18n.tr("Allowing for all sites will disable this setting")
                ,i18n.tr("When this is disabled, all new view requests will be allowed unless a domain is set to always deny. You also will never be asked regardless of domain-specific settings. ")
            ])
            itemType: Common.SettingsItem.ItemType.CheckBox
            bindProperty: "alwaysAskNewViewRequests"
        }
        , Common.SettingsItem {
            mainGroup: "privacy"
            subGroup: "permissions"
            title: i18n.tr("Blacklist and Whitelist")
            description: TextUtils.bulletTextArray(
            [
                i18n.tr("List of domains that are blacklisted or whitelisted")
                ,i18n.tr("Blacklisted sites will be rejected and cannot be navigated or visited")
                ,i18n.tr('Whitelisted sites can be navigated or visited when "Only allow whitelisted sites" is enabled')
            ])
            itemType: Common.SettingsItem.ItemType.Action
            itemFunction: function() {
                popupPageStack.openWithItem(domainPermissionsComponent)
            }
        }
        , Common.SettingsItem {
            mainGroup: "privacy"
            subGroup: "permissions"
            title: i18n.tr("Domain-specific settings")
            description: TextUtils.bulletTextArray(
            [
                i18n.tr("List of domains with specific settings which only applies to sites that are under the respective domains")
                ,i18n.tr("Settings may include custom user agent and default zoom ")
                ,i18n.tr("Custom user agents can also be managed from here")
            ])
            itemType: Common.SettingsItem.ItemType.Action
            itemFunction: function() {
                popupPageStack.openWithItem(domainSettingsComponent)
            }
        }
        , Common.SettingsItem {
            mainGroup: "privacy"
            subGroup: "permissions"
            visible: false // Doesn't work yet in QtWebEngine
            title: i18n.tr("Microphone")
            description: i18n.tr("Set the default microphone device")
            itemType: Common.SettingsItem.ItemType.DeviceSelector
            bindProperty: "defaultAudioDevice"
        }
        , Common.SettingsItem {
            mainGroup: "privacy"
            subGroup: "permissions"
            visible: false // Doesn't work yet in QtWebEngine
            title: i18n.tr("Camera")
            description: i18n.tr("Set the default camera device")
            itemType: Common.SettingsItem.ItemType.DeviceSelector
            bindProperty: "defaultVideoDevice"
        }
        , Common.SettingsItem {
            mainGroup: "reset"
            subGroup: "application"
            title: i18n.tr("Application")
            itemType: Common.SettingsItem.ItemType.Header
        }
        , Common.SettingsItem {
            mainGroup: "reset"
            subGroup: "application"
            title: i18n.tr("Reset application settings")
            description: i18n.tr("Resets all application settings to default values")
            itemType: Common.SettingsItem.ItemType.Action
            itemFunction: function() {
                let properties = {
                    title: title
                    , text: description
                    , confirmButtonText: i18n.tr("Reset settings")
                    , confirmIsPositive: false
                    , acceptFunction: function() { internal.resetAppSettings() }
                }
                internal.showConfirmationDialog(properties)
            }
        }
        , Common.SettingsItem {
            mainGroup: "reset"
            subGroup: "browser"
            title: i18n.tr("Browser")
            itemType: Common.SettingsItem.ItemType.Header
        }
        , Common.SettingsItem {
            mainGroup: "reset"
            subGroup: "browser"
            title: i18n.tr("Clear history")
            description: i18n.tr("Delete browsing history which includes all the sites you've visited")
            itemType: Common.SettingsItem.ItemType.Action
            itemFunction: function() {
                let properties = {
                    title: title
                    , text: description
                    , confirmButtonText: i18n.tr("Clear history")
                    , confirmIsPositive: false
                    , acceptFunction: function(_itemsToDelete) { internal.deleteAllHistory(_itemsToDelete) }
                    , optionsList: [
                        { "id": "browsingHistory", "type": "checkbox", "group": "Web", "title": "Browsing History"}
                        , { "id": "closedTabs", "type": "checkbox", "group": "Web", "title": "Closed Tabs History"}
                        , { "id": "closedWindows", "type": "checkbox", "group": "Web", "title": "Closed Windows History"}
                    ]
                }
                internal.showConfirmationDialog(properties)
            }
        }
        , Common.SettingsItem {
            mainGroup: "reset"
            subGroup: "browser"
            title: i18n.tr("Clear cache")
            description: i18n.tr("Clear cached files which includes cache accumulated from your browsing activities as well as the application cache. These are usually temporary and unnecessary files.")
            itemType: Common.SettingsItem.ItemType.Action
            itemFunction: function() {
                let properties = {
                    title: title
                    , text: i18n.tr("Clear cached files from your browsing activities and application cache.")
                    , confirmButtonText: i18n.tr("Clear cache")
                    , confirmIsPositive: false
                    , acceptFunction: function(_itemsToDelete) { settingsPage.clearCache(_itemsToDelete) }
                    , optionsList: [
                        { "id": "webLabel", "type": "label", "group": "Web", "title": "Web"}
                        , { "id": "httpCache", "type": "checkbox", "group": "Web", "title": "HTTP Cache"}
                        , { "id": "favicons", "type": "checkbox", "group": "Web", "title": "Favicons"}
                        , { "id": "webApplicationCache", "type": "checkbox", "group": "Web", "title": "Application Cache"}
                        , { "id": "webGPUCache", "type": "checkbox", "group": "Web", "title": "GPU Cache"}
                        , { "id": "webFileSystem", "type": "checkbox", "group": "Web", "title": "File System"}
                        , { "id": "webLocalStorage", "type": "checkbox", "group": "Web", "title": "Local Storage"}
                        , { "id": "webServiceWorker", "type": "checkbox", "group": "Web", "title": "Service Worker"}
                        , { "id": "webVisitedLinks", "type": "checkbox", "group": "Web", "title": "Visited Links"}
                        , { "id": "appLabel", "type": "label", "group": "App", "title": "App"}
                        , { "id": "appCache", "type": "checkbox", "group": "App", "title": "App Cache"}
                        , { "id": "tabPreviews", "type": "checkbox", "group": "App", "title": "Tab Previews"}
                    ]
                }
                internal.showConfirmationDialog(properties)
            }
        }
        , Common.SettingsItem {
            mainGroup: "reset"
            subGroup: "browser"
            title: i18n.tr("Clear all cookies")
            description: i18n.tr("Clear all cookies from your browsing activities. Cookies are small pieces of information stored by sites and is usually used for sessions and to identify you.")
            itemType: Common.SettingsItem.ItemType.Action
            itemFunction: function() {
                let properties = {
                    title: title
                    , text: description
                    , confirmButtonText: i18n.tr("Clear cookies")
                    , confirmIsPositive: false
                    , acceptFunction: function() { settingsPage.clearAllCookies }
                }
                internal.showConfirmationDialog(properties)
            }
        }
    ]
}
