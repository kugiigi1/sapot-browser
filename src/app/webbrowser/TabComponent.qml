/*
 * Copyright 2014-2017 Canonical Ltd.
 *
 * This file is part of morph-browser.
 *
 * morph-browser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * morph-browser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import QtQuick.Window 2.2
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import QtQuick.Controls 2.5 as QQC2
import QtQuick.Controls.Suru 2.2
import webbrowserapp.private 0.1
import webbrowsercommon.private 0.1
import QtWebEngine 1.5
import "../actions" as Actions
import ".." as Common

// FIXME: This component breaks encapsulation: it uses variables not defined in
// itself. However this is an acceptable tradeoff with regards to
// startup time performance. Indeed having this component defined as a separate
// QML file as opposed to inline makes it possible to cache its compiled form.

Component {
    id: tabComponent

    BrowserTab {
        id: browserTab
        anchors.fill: parent
        incognito: browser ? browser.incognito : false
        current: browser ? browser.tabsModel && browser.tabsModel.currentTab === this : false
        focus: current
        findInPageMargin: chrome.actualAutoHide ? chrome.height - Math.abs(chrome.y) : 0
        shortcutFindNextText: browser.shortcutFindNext.nativeText
        shortcutFindPreviousText: browser.shortcutFindPrevious.nativeText

        property var bottomEdgeHandle
        property var browser
        property var chrome
        property var chromeController
        property var contentHandlerLoader
        property var downloadDialogLoader
        property var filePickerLoader
        property var internal
        property var recentView
        property var tabsModel

        property bool webviewPulledDown: false

        function pullDownWebview() {
            webviewPulledDown = true
        }

        function pullUpWebview() {
            webviewPulledDown = false
        }

        Item {
            id: contextualMenuTarget
            visible: false
        }

        onCurrentChanged: pullUpWebview()

        webviewComponent: Common.WebViewImpl {
            id: webviewimpl

            property BrowserTab tab
            property QtObject rotateButtonObj
            readonly property bool current: tab.current
            readonly property bool rotateAvailable: browser.currentWebview && browser.currentWebview.isFullScreen
                                        && browser.thisWindow.rotationAngle !== rotation

            currentWebview: browser ? browser.currentWebview : null
            //filePicker: filePickerLoader ? filePickerLoader.item : null

            wide: browser.wide

            anchors.centerIn: parent
            anchors.verticalCenterOffset: browserTab.webviewPulledDown ? browserTab.height / 3 : 0
            width: rotation == 270 || rotation == 90 || rotation == -270 || rotation == -90 ? parent.height : parent.width
            height: rotation == 270 || rotation == 90 || rotation == -270 || rotation == -90 ? parent.width : parent.height
            
            Behavior on anchors.verticalCenterOffset {
                UbuntuNumberAnimation { duration: UbuntuAnimation.FastDuration }
            }

            Behavior on rotation {
                RotationAnimation { direction: RotationAnimation.Shortest }
            }

            focus: true

            // CHECKME: Removed parent.focus but not sure the impact
            // needed to be removed so find mode won't disable the webview
            enabled: current && !bottomEdgeHandle.dragging && !recentView.visible

            // FIXME: Workaround on some devices where rotation detection doesn't work the first time
            // the browser go into full screen
            onRotateAvailableChanged: {
                if (rotateAvailable && !browser.settings.autoVideoRotate) {
                    showRotateButton()
                }
            }

            Connections {
                target: browser.thisWindow
                onSensorOrientationChanged: {
                    if (rotateAvailable && !browser.settings.autoVideoRotate) {
                        showRotateButton()
                    }
                }
            }

            // Reset rotation when orientation isn't locked
            Connections {
                target: webviewimpl
                Screen.onOrientationChanged: rotation = 0
            }

            onContextMenuRequested: Common.Haptics.play()

            onRotationChanged: {
                if (rotation !== 0 && browser.currentWebview.isFullScreen) {
                    fullscreenExitHintComponent.createObject(webviewimpl)
                }
            }

            onJavaScriptConsoleMessage: {
                try {
                    var parsedData = JSON.parse(message)
                    switch (parsedData.type) {
                        case "VIDEO":
                            if (browser.thisWindow.sensorExists && browser.settings.autoVideoRotate) {
                                maybeRotateContent(parsedData.orientation)
                            }
                        break
                    }
                } catch (err) {}
            }

            function showRotateButton() {
                if (rotateButtonObj) {
                    rotateButtonObj.delayHide()
                } else {
                    rotateButtonObj = rotateButtonComponent.createObject(browserTab)
                }
            }

            function maybeRotateContent(videoOrientation) {
                var rotationAngle = 0
                
                if (Screen.orientation == Qt.LandscapeOrientation
                        && videoOrientation == "portrait")  {
                        rotationAngle = 270
                } else if (Screen.orientation == Qt.PortraitOrientation
                        && videoOrientation == "landscape") {
                        rotationAngle = 90
                }

                rotation = rotationAngle
            }

            function allowNewViewRequest(_destination, _url) {
                switch (_destination) {

                    case WebEngineView.NewViewInTab:
                        browser.openLinkInNewTabRequested(_url, false);
                        break;

                   case WebEngineView.NewViewInBackgroundTab:
                       browser.openLinkInNewTabRequested(_url, true);
                       break;

                   case WebEngineView.NewViewInWindow:
                   case WebEngineView.NewViewInDialog:
                       browser.openLinkInNewWindowRequested(_url, browser.incognito);
                       break;

                }
            }

            onNewViewRequested: function(request) {
                if (browser) {
                    let currentDomain = UrlUtils.extractHost(browser.currentWebview.url)
                    let newViewPreference = DomainSettingsModel.getNewViewPreference(currentDomain)

                    if (browser.settings && browser.settings.alwaysAskNewViewRequests
                            && newViewPreference === DomainSettingsModel.AskForNewViewRequest) {
                        let properties = {
                            destination: request.destination
                            , url: request.requestedUrl
                            , domain: currentDomain
                            , showRememberDecision: ! browser.incognito
                        }
                        let allowNewViewDialog = newViewRequestDialogComponent.createObject(browser, properties)
                        if (browser.wide) {
                            allowNewViewDialog.openNormal();
                        } else {
                            allowNewViewDialog.openBottom();
                        }
                    } else if (newViewPreference !== DomainSettingsModel.DenyNewViewRequest){
                        allowNewViewRequest(request.destination, request.requestedUrl)
                    } else {
                        browser.showTooltip(i18n.tr("Request for new view has been denied"), "BOTTOM", 3000)
                    } 
                }
            }

            Component {
                id: newViewRequestDialogComponent

                Common.AllowNewViewRequestDialog {
                    id: newViewRequestDialog

                    onAllow: webviewimpl.allowNewViewRequest(destination, url)
                    onAllowPermanentlyForDomain: {
                        DomainSettingsModel.setNewViewPreference(domain, DomainSettingsModel.AllowNewViewRequest)
                        webviewimpl.allowNewViewRequest(destination, url)
                    }
                    onAllowPermanentlyForAll: {
                        browser.settings.alwaysAskNewViewRequests = false
                        webviewimpl.allowNewViewRequest(destination, url)
                    }
                    onDenyPermanentlyForDomain: DomainSettingsModel.setNewViewPreference(domain, DomainSettingsModel.DenyNewViewRequest)
                }
            }

            /*
            locationBarController {
                height: chrome ? chrome.height : 0
                mode: chromeController ? chromeController.defaultMode : null
            }

            */

            //contextMenu: browser && browser.wide ? contextMenuWideComponent : contextMenuNarrowComponent
            /*
            onNewViewRequested: {
                var newTab = browser.createTab({"request": request})
                var setCurrent = true//(request.disposition == Oxide.NewViewRequest.DispositionNewForegroundTab)
                internal.addTab(newTab, setCurrent, tabsModel.indexOf(browserTab) + 1)
                if (setCurrent) tabContainer.forceActiveFocus()
            }

            onCloseRequested: prepareToClose()
            onPrepareToCloseResponse: {
                if (proceed) {
                    if (tab) {
                        var i = tabsModel.indexOf(tab);
                        if (i != -1) {
                            tabsModel.remove(i);
                        }

                        // tab.close() destroys the context so add new tab before destroy if required
                        if (tabsModel.count === 0) {
                            internal.openUrlInNewTab("", true, true)
                        }

                        tab.close()
                    } else if (tabsModel.count === 0) {
                        internal.openUrlInNewTab("", true, true)
                    }
                }
            }
*/
            QtObject {
                id: webviewInternal
                property url storedUrl: ""
                property bool titleSet: false
                property string title: ""
            }

            onLoadingChanged: {
                if (loadRequest.status === WebEngineLoadRequest.LoadSucceededStatus) {
                    chrome.findInPageMode = false
                    webviewInternal.titleSet = false
                    webviewInternal.title = title
                }

                if (webviewimpl.incognito) {
                    return
                }

                if (loadRequest.status === WebEngineLoadRequest.LoadSucceededStatus) {
                    webviewInternal.storedUrl = loadRequest.url
                    // note: at this point the icon is an empty string most times, not sure why (seems to be set after this event)
                    HistoryModel.add(loadRequest.url, title, (UrlUtils.schemeIs(icon, "image") && UrlUtils.hostIs(icon, "favicon")) ? icon.toString().substring(("image://favicon/").length) : icon)
                }

                // If the page has started, stopped, redirected, errored
                // then clear the cache for the history update
                // Otherwise if no title change has occurred the next title
                // change will be the url of the next page causing the
                // history entry to be incorrect (pad.lv/1603835)
                if (loadRequest.status === WebEngineLoadRequest.LoadFailedStatus) {
                    webviewInternal.titleSet = true
                    webviewInternal.storedUrl = ""
                }
            }
                        /*
            onTitleChanged: {
                if (!webviewInternal.titleSet && webviewInternal.storedUrl.toString()) {
                    // Record the title to avoid updating the history database
                    // every time the page dynamically updates its title.
                    // We don’t want pages that update their title every second
                    // to achieve an ugly "scrolling title" effect to flood the
                    // history database with updates.
                    webviewInternal.titleSet = true
                    if (webviewInternal.title != title) {
                        webviewInternal.title = title
                        HistoryModel.update(webviewInternal.storedUrl, title, icon)
                    }
                }
            }
            */
            onIconChanged: {

                if (webviewimpl.incognito) {
                    return
                }

                if ((icon.toString() !== '') && webviewInternal.storedUrl.toString()) {
                    HistoryModel.update(webviewInternal.storedUrl, webviewInternal.title, (UrlUtils.schemeIs(icon, "image") && UrlUtils.hostIs(icon, "favicon")) ? icon.toString().substring(("image://favicon/").length) : icon)
                }
            }
            property var certificateError
            function resetCertificateError() {
                certificateError = null
            }
            /*
            onCertificateError: {
                if (!error.isMainFrame || error.isSubresource) {
                    // Not a main frame document error, just block the content
                    // (it’s not overridable anyway).
                    return
                }
                if (internal.isCertificateErrorAllowed(error)) {
                    error.allow()
                } else {
                    certificateError = error
                    error.onCancelled.connect(webviewimpl.resetCertificateError)
                }
            }
            */

            onIsFullScreenChanged: {
                browserTab.pullUpWebview()
                if (isFullScreen) {
                    fullscreenExitHintComponent.createObject(webviewimpl)
                } else {
                    rotation = 0
                }
            }

            Component {
                id: rotateButtonComponent

                Common.CustomizedButton {
                    id: rotateButton

                    display: QQC2.AbstractButton.IconOnly

                    anchors {
                        bottom: parent.bottom
                        bottomMargin: units.gu(4)
                        horizontalCenter: parent.horizontalCenter
                    }

                    leftPadding: units.gu(2)
                    rightPadding: leftPadding
                    topPadding: leftPadding
                    bottomPadding: leftPadding
                    opacity: 0.5
                    radius: width / 2

                    icon {
                        name: "view-rotate"
                        width: units.gu(4)
                        height: units.gu(4)
                    }

                    onClicked: {
                        delayHide()

                        webviewimpl.rotation = browser.thisWindow.rotationAngle
                    }

                    function delayHide() {
                        hideDelay.restart()
                    }

                    Behavior on opacity {
                        UbuntuNumberAnimation {
                            duration: UbuntuAnimation.SlowDuration
                        }
                    }
                    onOpacityChanged: {
                        if (opacity == 0.0) {
                            rotateButton.destroy()
                        }
                    }

                    // Delay showing to prevent it from jumping up while the
                    // webview is being resized
                    visible: false
                    Timer {
                        running: true
                        interval: 250
                        onTriggered: {
                            if (webviewimpl.rotateAvailable || webviewimpl.orientationMismatch) {
                                rotateButton.visible = true
                            }
                        }
                    }

                    Timer {
                        id: hideDelay

                        running: rotateButton.visible
                        interval: 2000
                        onTriggered: rotateButton.opacity = 0
                    }

                    Connections {
                        target: webviewimpl
                        onIsFullScreenChanged: {
                            if (!target.isFullScreen) {
                                rotateButton.destroy()
                            }
                        }
                    }
                }
            }

            Component {
                id: fullscreenExitHintComponent

                Rectangle {
                    id: fullscreenExitHint
                    objectName: "fullscreenExitHint"

                    anchors {
                        horizontalCenter: webviewimpl.rotation !== 0 ? undefined : parent.horizontalCenter
                        left: webviewimpl.rotation == -90 ? parent.left : undefined
                        right: webviewimpl.rotation == 90 ? parent.right : undefined
                        margins: webviewimpl.rotation !== 0 ? units.gu(2) : 0
                        bottom: parent.bottom
                        bottomMargin : units.gu(5)
                    }
                    height: units.gu(6)
                    width: Math.min(units.gu(50), parent.width - units.gu(12))
                    radius: units.gu(1)
                    color: theme.palette.normal.backgroundSecondaryText
                    opacity: 0.85

                    Behavior on opacity {
                        UbuntuNumberAnimation {
                            duration: UbuntuAnimation.SlowDuration
                        }
                    }
                    onOpacityChanged: {
                        if (opacity == 0.0) {
                            fullscreenExitHint.destroy()
                        }
                    }

                    // Delay showing the hint to prevent it from jumping up while the
                    // webview is being resized (https://launchpad.net/bugs/1454097).
                    visible: false
                    Timer {
                        running: true
                        interval: 250
                        onTriggered: fullscreenExitHint.visible = true
                    }

                    Label {
                        color: theme.palette.normal.background
                        font.weight: Font.Light
                        fontSizeMode: Text.HorizontalFit
                        minimumPixelSize: Label.XSmall
                        textSize: Label.Large
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        anchors {
                            fill: parent
                            leftMargin: units.gu(2) + (webviewimpl.rotation == 0 || edgePointer.rightEdgeHint ? 0 : edgePointer.width)
                            rightMargin: units.gu(2) + (webviewimpl.rotation == 0 || !edgePointer.rightEdgeHint ? 0 : edgePointer.width)
                        }

                        text: bottomEdgeHandle.enabled && !browser.hasKeyboard
                              ? webviewimpl.rotation == 0 ? i18n.tr("Swipe from the bottom edge to exit")
                                    : i18n.tr("Swipe from this edge to exit")
                              : i18n.tr("Press ESC to exit full screen")
                    }

                    Icon {
                        id: edgePointer

                        readonly property bool rightEdgeHint: webviewimpl.rotation == 90

                        visible: webviewimpl.rotation !== 0
                        height: units.gu(4)
                        width: height
                        name: rightEdgeHint ? "next" : "previous"
                        anchors {
                            right: rightEdgeHint ? parent.right : undefined
                            left: rightEdgeHint ? undefined : parent.left
                            verticalCenter: parent.verticalCenter
                        }
                    }

                    Timer {
                        running: fullscreenExitHint.visible
                        interval: 3000
                        onTriggered: fullscreenExitHint.opacity = 0
                    }

                    Connections {
                        target: webviewimpl
                        onIsFullScreenChanged: {
                            if (!target.isFullScreen) {
                                fullscreenExitHint.destroy()
                            }
                        }

                        onRotationChanged: {
                            fullscreenExitHint.destroy()
                        }
                    }
                }
            }

            Common.WebviewSwipeHandler {
                id: leftSwipeGesture
                objectName: "leftSwipeGesture"

                z: 1
                webviewPullDownState: browserTab.webviewPulledDown
                enabled: browserTab.browser.settings && browserTab.browser.settings.enableWebviewPullDownGestures
                                && !browser.bigUIMode
                usePhysicalUnit: browserTab.browser.settings && browserTab.browser.settings.physicalForGestures
                width: physicalDotsPerInch * 0.2 // 0.2 inch
                anchors {
                    left: parent ? parent.left : undefined
                    top: parent ? parent.top : undefined
                    bottom: parent ? parent.bottom : undefined
                }

                onTrigger: {
                    if (webviewPullDownState) {
                        browserTab.pullUpWebview()
                    } else {
                        browserTab.pullDownWebview()
                    }
                }
            }

            Common.WebviewSwipeHandler {
                id: rightSwipeGesture
                objectName: "rightSwipeGesture"

                z: 1
                webviewPullDownState: browserTab.webviewPulledDown
                enabled: browserTab.browser.settings && browserTab.browser.settings.enableWebviewPullDownGestures
                                && !browser.bigUIMode
                usePhysicalUnit: browserTab.browser.settings && browserTab.browser.settings.physicalForGestures
                width: physicalDotsPerInch * 0.2 // 0.2 inch
                anchors {
                    right: parent ? parent.right : undefined
                    top: parent ? parent.top : undefined
                    bottom: parent ? parent.bottom : undefined
                }

                onTrigger: {
                    if (webviewPullDownState) {
                        browserTab.pullUpWebview()
                    } else {
                        browserTab.pullDownWebview()
                    }
                }
            }
        }
    }
}
