/*
 * Copyright 2013-2015 Canonical Ltd.
 *
 * This file is part of morph-browser.
 *
 * morph-browser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * morph-browser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3
import "Highlight.js" as Highlight

FocusScope {
    id: suggestions

    property var searchTerms
    property var models
    property bool wide: false
    property real topEmptySpace
    property bool focusedSearch: false
    property string searchType
    property bool focusedSearchItemMatch: false
    property bool incognito: false

    readonly property int count: models.reduce(internal.countItems, 0)
    readonly property alias contentHeight: suggestionsList.contentHeight

    signal activated(string itemType, var itemData)

    function selectFirstItem() {
        // TODO: Change to itemAtIndex after Qt 5.13+ upgrade
        suggestionsList.itemAt(0,0).activated()
    }

    Rectangle {
        anchors.fill: parent
        anchors.topMargin: suggestions.topEmptySpace
        radius: units.gu(1)
        color: suggestions.incognito ? theme.palette.normal.base : theme.palette.normal.background
    }

    Rectangle {
        anchors.fill: parent
        radius: units.gu(1)
        color: "transparent"
        border {
            color: suggestions.incognito ? theme.palette.normal.foreground : theme.palette.normal.base
            width: 1
        }
    }

    Behavior on height {
        UbuntuNumberAnimation { duration: UbuntuAnimation.SnapDuration }
    }

    clip: true

    UbuntuListView {
        id: suggestionsList
        objectName: "suggestionsList"
        anchors.fill: parent
        anchors.topMargin: suggestions.topEmptySpace
        focus: true
        clip: true

        model: models.reduce(function(list, model) {
            var modelItems = []

            // Models inheriting from QAbstractItemModel and JS arrays expose their
            // data differently, so we need to collect their items differently
            if (model.forEach) {
                model.forEach(function(item) { modelItems.push(item) })
            } else {
                for (var i = 0; i < model.count; i++) modelItems.push(model.get(i))
            }

            modelItems.forEach(function(item) {
                item["itemType"] = model.itemType
                item["typeicon"] = model.icon
                item["displayUrl"] = model.displayUrl

                if ( !(model.excludedTabs && model.itemType == "TABS" && model.excludedTabs.indexOf(item.tab) > -1) ) { // Exclude current tab
                    list.push(item)
                }
            })
            return list
        }, [])

        delegate: Suggestion {
            objectName: "suggestionDelegate_" + index
            width: suggestionsList.width
            title: selected ? modelData.title : Highlight.highlightTerms(modelData.title, searchTerms, theme.palette.normal.focus)
            subtitle: modelData.displayUrl ? (selected ? modelData.url :
                                                         Highlight.highlightTerms(modelData.url, searchTerms, theme.palette.normal.focus)) : ""
            itemType: modelData.itemType || ""
            icon: modelData.typeicon || ""
            favicon: modelData.icon || ""
            wide: suggestions.wide
            showEnterLabel: index == 0 && suggestions.focusedSearch && suggestions.searchType == modelData.itemType
            incognito: suggestions.incognito

            onShowEnterLabelChanged: {
                if (showEnterLabel) {
                    suggestions.focusedSearchItemMatch = true
                } else {
                    suggestions.focusedSearchItemMatch = false
                }
            }

            onActivated: {
                let passedData
                switch (itemType) {
                    case "TABS":
                        passedData = modelData.tab
                        break
                    default:
                        passedData = modelData.url
                        break
                }
                suggestions.activated(itemType, passedData)
            }
        }

        onModelChanged: currentIndex = -1
        onActiveFocusChanged: {
            if (activeFocus) {
                currentIndex = 0
            } else {
                currentIndex = -1
            }
        }
    }

    Scrollbar {
        flickableItem: suggestionsList
        align: Qt.AlignTrailing
    }

    QtObject {
        id: internal

        function countItems(total, model) {
            return total + (model.hasOwnProperty("length") ? model.length : model.count)
        }
    }
}
