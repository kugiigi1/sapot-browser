/*
 * Copyright 2014-2015 Canonical Ltd.
 *
 * This file is part of morph-browser.
 *
 * morph-browser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * morph-browser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2 as QQC2
import QtWebEngine 1.5
import ".." as Common

QQC2.SwipeDelegate {
    id: tabPreview

    property alias title: chrome.title
    property alias subtitle: chrome.subtitle
    property alias tabIcon: chrome.icon
    property alias incognito: chrome.incognito
    property alias previewContainer: previewContainer
    property real chromeHeight
    property var tab
    readonly property url url: tab ? tab.url : ""
    property bool isCurrentTab: false
    property bool isCurrentItem: false
    property bool swipeCommitState: Math.abs(swipe.position) > 0.5 // If swipe is in position for committing
    property bool isContextMenuHighlight: false
    property bool showPreviewImage: true
    
    readonly property bool webviewFailedToLoad: tab.webview && tab.webview.lastLoadFailed
                                                        && (tab.webview.LoadStatus !== WebEngineView.LoadSucceededStatus
                                                            || tab.webview.LoadStatus !== WebEngineView.LoadStartedStatus)

    background: Rectangle {
        color: "transparent"
    }
    leftPadding: 0
    rightPadding: 0
    topPadding: 0
    bottomPadding: 0
    swipe.enabled: true
    swipe.behind: Rectangle {
        width: tabPreview.width
        height: tabPreview.height
        color: "transparent"
    }

    swipe.onCompleted: closed()

    onSwipeCommitStateChanged: {
        if (swipeCommitState) {
            Common.Haptics.play()
        } else {
            Common.Haptics.playSubtle()
        }
    }
    onClicked: tabPreview.selected()
    onPressAndHold: tabPreview.contextMenu(false)

    signal selected()
    signal contextMenu(bool mouseClick)
    signal closed()
    signal mouseClicked()

    contentItem: Item {
        opacity: tabPreview.swipeCommitState ? Math.max(1 - Math.abs(tabPreview.swipe.position), 0.1) : 1
        Behavior on opacity {
            UbuntuNumberAnimation { duration: UbuntuAnimation.SnapDuration }
        }

        TabChrome {
            id: chrome

            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }
            tabWidth: tabPreview.width
            height: tabPreview.chromeHeight
            loading: tabPreview.tab && tabPreview.tab.webview && tabPreview.tab.webview.loading && tabPreview.tab.webview.loadProgress !== 100

            onSelected: tabPreview.selected()
            onClosed: tabPreview.closed()
        }

        Item {
            id: previewImage

            anchors {
                top: chrome.bottom
                topMargin: units.dp(-1)
                left: parent.left
                right: parent.right
            }

            visible: tabPreview.showPreviewImage && !tab.loadingPreview
            height: parent.height - chrome.height
            clip: true

            Rectangle {
                anchors.fill: parent
                color: theme.palette.normal.foreground
            }
            
            Rectangle {
                anchors {
                    top: parent.top
                    left: parent.left
                    right: parent.right
                }
                height: units.dp(1)

                color: theme.palette.normal.base
            }

            Image {
                visible: !previewContainer.visible
                source: "assets/tab-artwork.png"
                asynchronous: true
                fillMode: Image.PreserveAspectFit
                width: parent.width / 5
                height: width
                anchors {
                    right: parent.right
                    rightMargin: -width / 5
                    bottom: parent.bottom
                    bottomMargin: -height / 10
                }
            }

            Label {
                visible: !previewContainer.visible
                text: tabPreview.webviewFailedToLoad ? i18n.tr("Failed to load") : i18n.tr("Tap to view")
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WordWrap
                anchors {
                    left: parent.left
                    right: parent.right
                    verticalCenter: parent.verticalCenter
                }
            }

            Image {
                id: previewContainer
                visible: source.toString() && (status == Image.Ready) && !tabPreview.webviewFailedToLoad
                anchors.fill: parent
                anchors.topMargin: units.dp(1)
                verticalAlignment: Image.AlignTop
                fillMode: Image.PreserveAspectCrop
                source: tabPreview.showPreviewImage && tabPreview.tab ? tabPreview.tab.preview : ""
                asynchronous: true
                cache: false
                onStatusChanged: {
                    if (status != Image.Loading) {
                        tab.loadingPreview = false
                    }
                }
            }
        }

        Rectangle {
            anchors.fill: parent
            color: theme.palette.normal.focus
            visible: opacity > 0
            opacity: tabPreview.isCurrentItem || tabPreview.hovered || tabPreview.isContextMenuHighlight ? 0.1 : 0
            Behavior on opacity {
                UbuntuNumberAnimation { duration: UbuntuAnimation.FastDuration }
            }
        }

        Rectangle {
            anchors {
                fill: parent
                margins: -(border.width / 2)
            }
            color: "transparent"
            border.width: tabPreview.isCurrentItem || tabPreview.hovered || tabPreview.isContextMenuHighlight ? units.gu(1) : units.gu(0.8)
            border.color: tabPreview.isCurrentItem || tabPreview.isContextMenuHighlight ? theme.palette.normal.focus
                                                   : tabPreview.isCurrentTab ? "#FF762572"
                                                                             : theme.palette.normal.background
            radius: units.gu(1.5)
            Behavior on border.width {
                UbuntuNumberAnimation { duration: UbuntuAnimation.FastDuration }
            }
        }
    }

    // Mouse click handler
    TapHandler {
        id: mouseHandler
        acceptedButtons: Qt.RightButton | Qt.LeftButton | Qt.MiddleButton
        acceptedDevices: PointerDevice.Mouse

        onSingleTapped: {
            if (eventPoint.event.button === Qt.LeftButton) {
                tabPreview.selected()
            } else if (eventPoint.event.button === Qt.RightButton) {
                tabPreview.mouseClicked()
                tabPreview.contextMenu(true)
            } else if ((eventPoint.event.buttons === 0) && (eventPoint.event.button === Qt.MiddleButton)) {
                tabPreview.mouseClicked()
                tabPreview.closed()
            }
        }
        onDoubleTapped: tabPreview.mouseClicked()
    }
}
