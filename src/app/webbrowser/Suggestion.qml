/*
 * Copyright 2015 Canonical Ltd.
 *
 * This file is part of morph-browser.
 *
 * morph-browser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * morph-browser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.12
import ".."

ListItem {
    id: suggestionItem

    property string title
    property string subtitle
    property alias icon: icon.name
    property alias favicon: favicon.source
    property string itemType
    property bool wide: false
    property bool showEnterLabel: false
    property bool incognito: false

    signal activated()

    divider.visible: false
    height: Math.max(middleVisuals.height, units.gu(6))
    // disable focus handling
    activeFocusOnPress: false

    onClicked: suggestionItem.activated()

    ListItemLayout {
        id: middleVisuals

        title.text: suggestionItem.title
        subtitle.text: suggestionItem.subtitle
        title.color: theme.palette.normal.foregroundText

        RowLayout {
            SlotsLayout.position: SlotsLayout.Leading
            SlotsLayout.overrideVerticalPositioning: true
            anchors {
                top: parent.top
                bottom: parent.bottom
            } 
            Icon {
                id: icon
                Layout.alignment: Qt.AlignVCenter
                visible: itemType == "SEARCH"
                width: units.gu(2)
                height: width
                color: suggestionItem.incognito ? theme.palette.normal.baseText : theme.palette.normal.backgroundText
                asynchronous: true
            }

            Favicon {
                id: favicon
                Layout.alignment: Qt.AlignVCenter
                visible: !icon.visible
                shouldCache: !browser.incognito
                width: units.gu(2)
                height: width
            }
        }

        RowLayout {
            SlotsLayout.position: SlotsLayout.Trailing
            SlotsLayout.overrideVerticalPositioning: true
            visible: !icon.visible
            anchors {
                top: parent.top
                bottom: parent.bottom
            }

            Item {
                Layout.alignment: Qt.AlignVCenter
                width: trailingBackground.width

                Rectangle {
                    id: trailingBackground
                    anchors.verticalCenter: parent.verticalCenter
                    color: theme.palette.normal.foreground
                    radius: units.gu(1.5)
                    width: itemTypeLayout.width + units.gu(2)
                    height: units.gu(4)
                }

                RowLayout {
                    id: itemTypeLayout
                    anchors.centerIn: parent

                    Icon {
                        id: trailingIcon
                        Layout.alignment: Qt.AlignVCenter
                        name: icon.name
                        width: units.gu(2)
                        height: width
                        color: theme.palette.normal.foregroundText
                        asynchronous: true
                    }

                    Label {
                        Layout.alignment: Qt.AlignVCenter
                        text: {
                            switch (itemType) {
                                case "BOOKMARKS":
                                    return i18n.tr("Bookmark")
                                case "HISTORY":
                                    return i18n.tr("History")
                                case "TABS":
                                    return i18n.tr("Switch to tab")
                                default:
                                    return ""
                            }
                        }
                        visible: suggestionItem.wide
                    }
                }
            }

            Label {
                Layout.alignment: Qt.AlignVCenter
                visible: suggestionItem.showEnterLabel
                text: suggestionItem.wide ? i18n.tr("↵ Enter") : i18n.tr("↵")
                verticalAlignment: Text.AlignVCenter
                color: suggestionItem.incognito ? theme.palette.normal.baseText : theme.palette.normal.backgroundText
            }
        }
    }

    MouseArea {
        id: hover

        hoverEnabled: true
        acceptedButtons: Qt.LeftButton
        anchors.fill: parent
        onClicked: suggestionItem.activated()
    }

    Rectangle {
        id: hoverBg

        anchors.fill: parent
        opacity: 0.2
        visible: (hover.containsMouse || suggestionItem.showEnterLabel) && !suggestionItem.ListView.isCurrentItem
        color: theme.palette.normal.focus
    }
}
