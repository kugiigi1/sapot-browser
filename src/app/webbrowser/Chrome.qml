/*
 * Copyright 2013-2017 Canonical Ltd.
 *
 * This file is part of morph-browser.
 *
 * morph-browser is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * morph-browser is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3
import QtWebEngine 1.5
import ".."

ChromeBase {
    id: chrome

    property var tabsModel
    property alias wide: navigationBar.wide
    property alias tab: navigationBar.tab
    property alias navHistoryOpen: navigationBar.navHistoryOpen
    property alias searchUrl: navigationBar.searchUrl
    property alias text: navigationBar.text
    property alias actualUrl: navigationBar.actualUrl
    readonly property alias searchType: navigationBar.searchType
    readonly property alias focusedSearch: navigationBar.focusedSearch
    property alias bookmarked: navigationBar.bookmarked
    signal closeTabRequested()
    signal toggleBookmark()
    signal toggleDownloads(var caller)
    property bool showDownloadButton: false
    property bool downloadNotify: false
    property alias bigUIMode: navigationBar.bigUIMode
    property alias drawerMenu: navigationBar.drawerMenu
    property alias drawerOpen: navigationBar.drawerOpen
    property alias leftMenuOpen: navigationBar.leftMenuOpen
    property alias rightMenuOpen: navigationBar.rightMenuOpen
    property alias requestedUrl: navigationBar.requestedUrl
    property alias canSimplifyText: navigationBar.canSimplifyText
    property alias findInPageMode: navigationBar.findInPageMode
    property alias tabListMode: navigationBar.tabListMode
    property alias contextMenuVisible: navigationBar.contextMenuVisible
    property alias editing: navigationBar.editing
    property alias incognito: navigationBar.incognito
    property alias showTabsBar: tabsBar.active
    property alias showFaviconInAddressBar: navigationBar.showFaviconInAddressBar
    property alias availableHeight: navigationBar.availableHeight
    property alias searchEngine: navigationBar.searchEngine
    property alias availableHeightBelowChrome: navigationBar.availableHeightBelowChrome
    property alias hideTabsButtonNavBar: navigationBar.hideTabsButtonNavBar
    property alias enableFocusedSearch: navigationBar.enableFocusedSearch
    property alias searchSuggestionsSettings: navigationBar.searchSuggestionsSettings
    readonly property alias bookmarkTogglePlaceHolder: navigationBar.bookmarkTogglePlaceHolder
    readonly property alias downloadsButtonPlaceHolder: navigationBar.downloadsButtonPlaceHolder
    property bool touchEnabled: true
    readonly property bool contextTabMenuOpen: tabsBar.item && tabsBar.item.contextTabMenuOpen
    readonly property real tabsBarHeight: tabsBar.height + tabsBar.anchors.topMargin + content.anchors.topMargin
    property BrowserWindow thisWindow
    property Component windowFactory
    property var windowFactoryProperties: tabsBar.item ? tabsBar.item.windowFactoryProperties : undefined
    property bool tabsBarDimmed: false

    signal switchToTab(int index)
    signal requestNewTab(string url, int index, bool makeCurrent, var sourceTab)
    signal tabClosed(int index, bool moving)
    signal openRecentView
    signal suggestionSwitchTab(int tabIndex)
    signal suggestionsActivated(var itemData)
    signal suggestionsEscaped
    signal downPressed

    webview: tab ? tab.webview : null
    backgroundColor: incognito ? UbuntuColors.darkGrey : theme.palette.normal.background

    implicitHeight: tabsBar.height + navigationBar.height + content.anchors.topMargin

    function selectAll() {
        navigationBar.selectAll()
    }

    function triggerLeftAction(fromBottom) {
        navigationBar.triggerLeftAction(fromBottom)
    }

    function triggerRightAction(fromBottom) {
        navigationBar.triggerRightAction(fromBottom)
    }

    function showBackNavHistory(fromBottom, caller) {
        navigationBar.showNavHistory(webview.navigationHistory.backItems, fromBottom, caller)
    }

    function showForwardNavHistory(fromBottom, caller) {
        navigationBar.showNavHistory(webview.navigationHistory.forwardItems, fromBottom, caller)
    }

    function openDrawerFromBottom() {
        navigationBar.openDrawerFromBottom()
    }

    onEditingChanged: {
        if (editing) {
            state = "shown"
        } else {
            chrome.changeChromeState("shown")
        }
    }

    Keys.onDownPressed: {
        if (navigationBar.suggestionsCount && navigationBar.showSuggestions) {
            navigationBar.suggestionsFocus = true
        } else {
            downPressed()
        }
    }

    FocusScope {
        id: content
        anchors.fill: parent

        focus: true

        Rectangle {
            anchors.fill: navigationBar
            color: !incognito ? theme.palette.normal.background : theme.palette.normal.base
        }

        Loader {
            id: tabsBar
            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
            }
            asynchronous: true
            height: active ? units.gu(4) : 0

            Component.onCompleted: {
                setSource(
                    Qt.resolvedUrl("TabsBar.qml"),
                    {
                        "dimmed": Qt.binding(function() { return chrome.tabsBarDimmed; }),
                        "model": Qt.binding(function() { return chrome.tabsModel; }),
                        "incognito": Qt.binding(function() { return chrome.incognito; }),
                        "dragAndDrop.previewTopCrop": Qt.binding(function() { return chrome.height; }),
                        "dragAndDrop.thisWindow": Qt.binding(function() { return chrome.thisWindow; }),
                        "windowFactory": Qt.binding(function() { return chrome.windowFactory; }),
                    }
                )
            }

            Connections {
                target: tabsBar.item

                onRequestNewTab: chrome.requestNewTab(url, index, makeCurrent, sourceTab)
                onTabClosed: chrome.tabClosed(index, moving)
                onOpenRecentView: chrome.openRecentView()
            }
        }

        ThinDivider {
            anchors {
                left: navigationBar.left
                right: navigationBar.right
                bottom: navigationBar.bottom
            }
        }

        NavigationBar {
            id: navigationBar

            loading: chrome.loading
            fgColor: theme.palette.normal.backgroundText
            iconColor: (incognito && !showTabsBar) ? theme.palette.normal.baseText : fgColor
            showDownloadButton: chrome.showDownloadButton
            downloadNotify: chrome.downloadNotify
            editing: chrome.activeFocus || suggestionsActiveFocus
            tabsModel: chrome.tabsModel
            showSuggestions: ((chrome.state === "shown") && (suggestionsActiveFocus || chrome.activeFocus) &&
                              (suggestionsCount > 0) && !chrome.drawerOpen && !chrome.findInPageMode && !chrome.contextMenuVisible
                              && chrome.text !== chrome.actualUrl)

            focus: true

            anchors {
                bottom: parent.bottom
                left: parent.left
                right: parent.right
            }

            height: chrome.bigUIMode ? units.gu(8) : units.gu(6)

            onCloseTabRequested: chrome.closeTabRequested()
            onToggleBookmark: chrome.toggleBookmark()
            onToggleDownloads: chrome.toggleDownloads(caller)
            onSuggestionSwitchTab: chrome.suggestionSwitchTab(tabIndex)
            onSuggestionsActivated: chrome.suggestionsActivated(itemData)
            onSuggestionsEscaped: chrome.suggestionsEscaped()
            onOpenRecentView: chrome.openRecentView()
        }
    }

    // Delay changing the 'loading' state, to allow for very brief load
    // sequences to not update the UI, which would result in inelegant
    // flickering (https://launchpad.net/bugs/1611680).
    Connections {
        target: webview
        onLoadingChanged: delayedLoadingNotifier.restart()
    }
    Timer {
        id: delayedLoadingNotifier
        interval: 100
        onTriggered: loading = webview.loading && webview.loadProgress !== 100
    }

    loadProgress: (loading && webview) ? webview.loadProgress : 0

    // If the webview changes the use the loading state of the new webview
    // otherwise opening a new tab/window while another webview was loading
    // can cause a progress bar to be left behind at zero percent pad.lv/1638337
    onWebviewChanged: loading = webview ? webview.loading &&
                                          webview.loadProgress !== 100 : false
}
