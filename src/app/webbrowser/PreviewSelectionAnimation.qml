import QtQuick 2.4
import Ubuntu.Components 1.3

Image {
    id: previewSelectionAnimation

    readonly property int animationDuration: UbuntuAnimation.BriskDuration
    readonly property alias sourceIndex: internal.sourceIndex
    property real targetY
    property real targetHeight

    z: 100
    visible: source.toString() && (status == Image.Ready)
    verticalAlignment: Image.AlignTop
    fillMode: Image.PreserveAspectCrop
    asynchronous: true
    cache: false

    signal selectionStart
    signal selectionEnd

    function startAnimation(tabIndex, sourceUrl, sourceContainer, fadeOut) {
        internal.sourceIndex = tabIndex
        internal.fadeOut = fadeOut
        source = sourceUrl
        var startingPos = sourceContainer.mapToItem(parent, 0, 0)
        x = startingPos.x
        y = startingPos.y
        opacity = 1
        width = sourceContainer.width
        height = sourceContainer.height
        selectionAnimation.restart()

        selectionStart()
    }

    QtObject {
        id: internal

        property int sourceIndex
        property bool fadeOut: false
    }

    ParallelAnimation {
        id: selectionAnimation

        running: false

        onStopped: {
            previewSelectionAnimation.selectionEnd()
            previewSelectionAnimation.source = ""
        }

        UbuntuNumberAnimation {
            target: previewSelectionAnimation
            property: "y"
            to: previewSelectionAnimation.targetY
            duration: previewSelectionAnimation.animationDuration
            easing: UbuntuAnimation.StandardEasing
        }

        UbuntuNumberAnimation {
            target: previewSelectionAnimation
            property: "x"
            to: 0
            duration: previewSelectionAnimation.animationDuration
            easing: UbuntuAnimation.StandardEasing
        }

        UbuntuNumberAnimation {
            target: previewSelectionAnimation
            property: "opacity"
            to: internal.fadeOut ? 0 : 1
            duration: previewSelectionAnimation.animationDuration
            easing: UbuntuAnimation.StandardEasingReverse
        }

        UbuntuNumberAnimation {
            target: previewSelectionAnimation
            property: "width"
            to: parent.width
            duration: previewSelectionAnimation.animationDuration
            easing: UbuntuAnimation.StandardEasing
        }

        UbuntuNumberAnimation {
            target: previewSelectionAnimation
            property: "height"
            to: previewSelectionAnimation.targetHeight
            duration: previewSelectionAnimation.animationDuration
            easing: UbuntuAnimation.StandardEasing
        }
    }
}
