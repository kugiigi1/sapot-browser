import QtQuick 2.9
import QtQuick.Controls 2.5 as QQC2
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.12
import "." as Common

QQC2.Page {
    id: basePageStack

    property alias headerExpanded: pageHeader.expanded
    property alias pageHeader: pageHeader
    property alias customTitleItem: pageHeader.customTitleItem
    property alias initialItem: mainStackView.initialItem
    property alias currentItem: mainStackView.currentItem
    property alias depth: mainStackView.depth
    property alias stackView: mainStackView
    property var defaultLeftActions: []
    property var defaultRightActions: []

    function push(item) {
        mainStackView.push(item)
    }

    function pop() {
        mainStackView.pop()
    }

    function clear() {
        mainStackView.clear()
    }

    clip: true
    focus: true

    header: Common.CustomizedPageHeader {
        id: pageHeader

        expandable: browser.settings.appPagesPullDown && basePageStack.parent.height >= units.gu(60) && !browser.bigUIMode
        currentItem: mainStackView.currentItem
        leftActions: currentItem && currentItem.headerLeftActions
                        ? [...basePageStack.defaultLeftActions, ...currentItem.headerLeftActions]
                        : basePageStack.defaultLeftActions
        rightActions: currentItem && currentItem.headerRightActions
                        ? [...currentItem.headerRightActions, ...basePageStack.defaultRightActions]
                        : basePageStack.defaultRightActions
    }

    QQC2.StackView {
        id: mainStackView

        focus: true
        anchors {
            fill: parent
            bottomMargin: browser.bigUIMode ? units.gu(browser.settings.bottomGesturesAreaHeightBigUIMode) : 0
        }
    }

    Loader {
        active: browser.bigUIMode
        asynchronous: true
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }
        sourceComponent: Rectangle {
            color: theme.palette.normal.foreground
            height: units.gu(browser.settings.bottomGesturesAreaHeightBigUIMode)
        }
    }

    // Swipe area for reverting back the header to default height
    SwipeArea {
        id: swipeHeaderRevert

        z: mainStackView.z + 1
        enabled: pageHeader.expanded
        anchors.fill: parent
        immediateRecognition: false
        grabGesture: true
        direction: SwipeArea.Upwards

        onDraggingChanged: {
            if (!dragging && pageHeader.expanded && pageHeader.height <= pageHeader.maxHeight - pageHeader.expansionThreshold * 2) {
                pageHeader.expanded = false
            }
        }

        onDistanceChanged: {
            if (dragging && pageHeader.height > pageHeader.defaultHeight
                    && pageHeader.height <= pageHeader.maxHeight
                ) {

                let newValue = pageHeader.maxHeight - distance

                switch (true) {
                    case newValue <= pageHeader.defaultHeight:
                        pageHeader.height = pageHeader.defaultHeight
                        break
                    case newValue >= pageHeader.maxHeight:
                        pageHeader.height = pageHeader.maxHeight
                        break
                    default:
                        pageHeader.height = newValue
                        break
                }
            }
        }
    }

    Common.GoIndicator {
        id: goForwardIcon

        iconName: enabled && pageHeader.rightVisibleActionsCount > 1 ? "navigation-menu"
                                    : pageHeader.rightVisibleAction ? pageHeader.rightVisibleAction.iconName
                                                                    : ""
        swipeProgress: bottomBackForwardHandle.swipeProgress
        enabled: pageHeader.rightVisibleActionsCount > 0
        anchors {
            right: parent.right
            rightMargin: units.gu(3)
            verticalCenter: parent.verticalCenter
        }
    }

    Common.GoIndicator {
        id: goBackIcon

        iconName: enabled && pageHeader.leftVisibleActionsCount > 1 ? "navigation-menu"
                                    : pageHeader.leftVisibleAction ? pageHeader.leftVisibleAction.iconName
                                                                   : ""
        swipeProgress: bottomBackForwardHandle.swipeProgress
        enabled: pageHeader.leftVisibleActionsCount > 0
        anchors {
            left: parent.left
            leftMargin: units.gu(3)
            verticalCenter: parent.verticalCenter
        }
    }

    ActivityIndicator {
        id: busyIndicator

        anchors.centerIn: parent
        running: basePageStack.currentItem ? false : true
        opacity: running ? 1 : 0
        visible: opacity > 0
        Behavior on opacity { UbuntuNumberAnimation {} }
    }

    RowLayout {
        id: bottomRowLayout

        property real sideSwipeAreaWidth: basePageStack.width * (basePageStack.width > basePageStack.height ? 0.15 : 0.30)

        spacing: 0
        z: swipeHeaderRevert.z + 1
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        opacity: basePageStack.currentItem ? 1 : 0
        visible: opacity > 0
        Behavior on opacity { UbuntuNumberAnimation {} }

        Common.BottomSwipeArea {
            Layout.fillHeight: true
            Layout.fillWidth: !horizontalSwipeItem.visible
            Layout.preferredWidth: bottomRowLayout.sideSwipeAreaWidth
            Layout.alignment: Qt.AlignBottom | Qt.AlignLeft

            enabled: browser.settings.appPagesSideSwipe
            model: pageHeader.leftActions
            enableQuickActions: browser.settings.appPagesDirectActions
            enableQuickActionsDelay: browser.settings.appPagesQuickSideSwipe
                                                || (!browser.settings.appPagesQuickSideSwipe && browser.settings.appPagesQuickActionEnableDelay)
            triggerSignalOnQuickSwipe: true
            edge: Common.BottomSwipeArea.Edge.Left
            maxQuickActionsHeightInInch: browser.settings.appPagesQuickActionsHeight
            availableHeight: mainStackView.height
            availableWidth: mainStackView.width
            implicitHeight: browser.bigUIMode ? units.gu(browser.settings.bottomGesturesAreaHeightBigUIMode)
                                              : units.gu(browser.settings.bottomGesturesAreaHeight)

            onTriggered: {
                if (!browser.settings.appPagesDirectActions
                        || (browser.settings.appPagesDirectActions && browser.settings.appPagesQuickSideSwipe)) {
                    pageHeader.triggerLeftFromBottom()
                    Common.Haptics.play()
                }
            }
        }
        
        Item {
            id: horizontalSwipeItem

            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.alignment: Qt.AlignBottom

            visible: browser.settings.appPagesHorizontalSwipe

            MouseArea {
                id: bottomEdgeHint

                readonly property alias color: recVisual.color
                readonly property real defaultHeight: units.gu(0.5)

                hoverEnabled: true
                height: browser.bigUIMode ? units.gu(browser.settings.bottomGesturesAreaHeightBigUIMode)
                                          : defaultHeight

                anchors {
                    left: parent.left
                    right: parent.right
                    bottom: parent.bottom
                    bottomMargin: browser.bigUIMode ? 0 : defaultHeight
                }

                Behavior on opacity { UbuntuNumberAnimation {} }

                Rectangle {
                    id: recVisual

                    visible: browser.bigUIMode ? browser.settings.bigUIModeBottomHint : browser.settings.appPagesHorizontalSwipeHint
                    color: bottomEdgeHint.containsMouse ? UbuntuColors.silk : UbuntuColors.ash
                    radius: height / 2
                    height: bottomEdgeHint.containsMouse ? units.gu(1) : units.gu(0.5)
                    anchors {
                        verticalCenter: parent.verticalCenter
                        left: parent.left
                        right: parent.right
                    }
                }
            }

            Common.HorizontalSwipeHandle {
                id: bottomBackForwardHandle

                leftAction: goBackIcon
                rightAction: goForwardIcon
                immediateRecognition: true
                usePhysicalUnit: browser.settings.physicalForGestures
                height: browser.bigUIMode ? units.gu(browser.settings.bottomGesturesAreaHeightBigUIMode)
                                          : units.gu(browser.settings.bottomGesturesAreaHeight)
                swipeHoldDuration: 700
                anchors {
                    left: parent.left
                    right: parent.right
                    bottom: parent.bottom
                }

                rightSwipeHoldEnabled: false
                leftSwipeHoldEnabled: false

                onRightSwipe:  pageHeader.triggerLeftFromBottom()
                onLeftSwipe:  pageHeader.triggerRightFromBottom()
                onPressedChanged: if (pressed) Common.Haptics.playSubtle()
            }
        }

        Common.BottomSwipeArea {
            Layout.fillHeight: true
            Layout.fillWidth: !horizontalSwipeItem.visible
            Layout.preferredWidth: bottomRowLayout.sideSwipeAreaWidth
            Layout.alignment: Qt.AlignBottom | Qt.AlignRight

            enabled: browser.settings.appPagesSideSwipe
            model: pageHeader.rightActions
            enableQuickActions: browser.settings.appPagesDirectActions
            enableQuickActionsDelay: browser.settings.appPagesQuickSideSwipe
                                                || (!browser.settings.appPagesQuickSideSwipe && browser.settings.appPagesQuickActionEnableDelay)
            triggerSignalOnQuickSwipe: true
            edge: Common.BottomSwipeArea.Edge.Right
            maxQuickActionsHeightInInch: browser.settings.appPagesQuickActionsHeight
            availableHeight: mainStackView.height
            availableWidth: mainStackView.width
            implicitHeight: browser.bigUIMode ? units.gu(browser.settings.bottomGesturesAreaHeightBigUIMode)
                                              : units.gu(browser.settings.bottomGesturesAreaHeight)

            onTriggered: {
                if (!browser.settings.appPagesDirectActions
                        || (browser.settings.appPagesDirectActions && browser.settings.appPagesQuickSideSwipe)) {
                    pageHeader.triggerRightFromBottom()
                    Common.Haptics.play()
                }
            }
        }
    }
}
