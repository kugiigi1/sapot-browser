import QtQuick 2.12
import QtQuick.Controls 2.12 as QQC2
import QtQuick.Layouts 1.3
import Ubuntu.Components 1.3
import "." as Common

Item {
    id: firstRunWizard

    property int currentStage: 0

    signal finished
    signal showTooltip(string text)

    Rectangle {
        color: UbuntuColors.jet
        opacity: 0.8
        anchors.fill: parent
    }

    MouseArea {
        anchors.fill: parent
    }

    ColumnLayout {
        anchors {
            left: parent.left
            right: parent.right
            verticalCenter: parent.verticalCenter
        }

        Label {
            Layout.fillWidth: true
            Layout.maximumWidth: units.gu(60)
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.margins: units.gu(2)
            
            font.pixelSize: units.gu(3)
            text: {
                switch (firstRunWizard.currentStage) {
                    case 0:
                        return i18n.tr("This app supports various bottom gestures for ease of use on touchscreen devices.")
                            + " " + i18n.tr("Spam click on the hard-to-click tiny close button at the bottom or click Donate to easily quit this screen.")
                    case 1:
                        return i18n.tr("Swipe up from the leftmost and rightmost sides of the bottom edge to perform actions.")
                            + " " + i18n.tr("Try it now and do a quick swipe up and a swipe up without lifting and see what they do.")
                    case 2:
                        return i18n.tr("Swipe horizontally on the visual hint at the bottom to perform actions as alternative to the swipe up gestures.")
                            + " " + i18n.tr("Try it now and swipe to the left or right and see what they do.")
                    case 3:
                        return i18n.tr("When in the web view or while browsing, perform a long swipe up from the middle part of the bottom edge to open the tabs list or short swipe to switch back to the previous tab.")
                            + " " + i18n.tr("Try it later in the app-proper and see what they do. I'm tired.")
                    case 4:
                        return i18n.tr("Remember, swiping will give you visual and/or haptic feedback but the action won't be performed until you lift the swipe.")
                            + " " + i18n.tr("You can always cancel the swipe gesture by swiping back to the starting position of the swipe.")
                    case 5:
                        return i18n.tr('Check "Settings > Gestures" to see all available options.')
                            + " " + i18n.tr("From there, click or hover on the question mark icon to see further details.")
                            + "\n\n" + i18n.tr("Salamat!")
                }
                return ""
            }
            wrapMode: Text.Wrap
            color: "white"
            horizontalAlignment: Text.AlignHCenter
        }
        
        ColumnLayout {
            Layout.alignment: Qt.AlignHCenter
            spacing: units.gu(2)

            QQC2.Button {
                id: closeButton

                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                visible: !donateButton.visible
                text: i18n.tr("Close")
                icon {
                    name: "close"
                    width: units.gu(2)
                    height: units.gu(2)
                }

                onClicked: firstRunWizard.finished()
            }

            QQC2.Button {
                id: donateButton

                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                text: i18n.tr("Donate") + "!!".repeat(firstRunWizard.currentStage)
                icon {
                    name: "like"
                    width: units.gu(2)
                    height: units.gu(2)
                }

                onClicked: visible = false
            }

            QQC2.Button {
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter

                visible: firstRunWizard.currentStage < 5
                text: firstRunWizard.currentStage == 0 ? i18n.tr("Show me how") : i18n.tr("Next")
                icon {
                    name: "go-next"
                    width: units.gu(2)
                    height: units.gu(2)
                }

                onClicked: firstRunWizard.currentStage += 1
            }
        }
    }

    ColumnLayout {
        anchors {
            horizontalCenter: parent.horizontalCenter
            bottom: parent.bottom
            bottomMargin: units.gu(10)
        }

        Icon {
            Layout.alignment: Qt.AlignHCenter
            implicitWidth: units.gu(1)
            implicitHeight: implicitWidth

            name: "close"
            color: "white"

            MouseArea {
                anchors.centerIn: parent
                width: units.gu(0.5)
                height: width
                onClicked: firstRunWizard.finished()
            }
        }

        Label {
            Layout.alignment: Qt.AlignHCenter
            text: i18n.tr("Close")
            font.pixelSize: units.gu(1)
            color: "white"
        }
    }
    
    Common.CustomizedPageHeader {
        id: pageHeader

        anchors.bottom: parent.top
        currentItem: firstRunWizard
        expandable: true
        leftActions: [
            Common.BaseAction {
                iconName: "go-previous"
                text: i18n.tr("Go back")
                onTrigger: firstRunWizard.showTooltip(text)
            }
        ]
        rightActions: [
            Common.BaseAction {
                iconName: "find"
                text: i18n.tr("Search")
                onTrigger: firstRunWizard.showTooltip(text)
            }
            , Common.BaseAction {
                iconName: "add"
                text: i18n.tr("Add new entry")
                onTrigger: firstRunWizard.showTooltip(text)
            }
            ,Common.BaseAction {
                iconName: "reload"
                text: i18n.tr("Refresh page")
                onTrigger: firstRunWizard.showTooltip(text)
            }
        ]
    }
    
    Common.GoIndicator {
        id: goForwardIcon

        iconName: enabled && pageHeader.rightVisibleActionsCount > 1 ? "navigation-menu"
                                    : pageHeader.rightVisibleAction ? pageHeader.rightVisibleAction.iconName
                                                                    : ""
        enabled: pageHeader.rightVisibleActionsCount > 0
        anchors {
            right: parent.right
            rightMargin: units.gu(3)
            verticalCenter: parent.verticalCenter
        }
    }

    Common.GoIndicator {
        id: goBackIcon

        iconName: enabled && pageHeader.leftVisibleActionsCount > 1 ? "navigation-menu"
                                    : pageHeader.leftVisibleAction ? pageHeader.leftVisibleAction.iconName
                                                                   : ""
        enabled: pageHeader.leftVisibleActionsCount > 0
        anchors {
            left: parent.left
            leftMargin: units.gu(3)
            verticalCenter: parent.verticalCenter
        }
    }
    
    Component {
        id: edgeHintComponent

        Rectangle {
            id: edgeHint

            color: UbuntuColors.blue

            PropertyAnimation { 
                id: appearAnimation
                
                running: visible
                target: edgeHint
                alwaysRunToEnd: true
                property: "opacity"
                to: 1
                duration: UbuntuAnimation.SlowDuration
                easing: UbuntuAnimation.StandardEasing
                onStopped: hideAnimation.start()
            }

            PropertyAnimation {
                 id: hideAnimation
                 
                 target: edgeHint
                 alwaysRunToEnd: true
                 property: "opacity"
                 to: 0
                 duration: UbuntuAnimation.SlowDuration
                 easing: UbuntuAnimation.StandardEasing
                 onStopped: appearAnimation.start()
             }
        }
    }

    // Stages
    Loader {
        active: firstRunWizard.currentStage == 0
        asynchronous: true
        height: units.gu(2)
        anchors{
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        sourceComponent: edgeHintComponent
    }

    Loader {
        active: firstRunWizard.currentStage >= 1
        asynchronous: true
        height: units.gu(2)
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        sourceComponent: Component {
            RowLayout {
                id: bottomRowLayout

                property real sideSwipeAreaWidth: firstRunWizard.width * (firstRunWizard.width > firstRunWizard.height ? 0.15 : 0.30)

                spacing: 0

                Common.BottomSwipeArea {
                    Layout.fillHeight: true
                    Layout.fillWidth: !horizontalSwipeItem.visible
                    Layout.preferredWidth: bottomRowLayout.sideSwipeAreaWidth
                    Layout.alignment: Qt.AlignBottom | Qt.AlignLeft

                    model: pageHeader.leftActions
                    enableQuickActions: true
                    triggerSignalOnQuickSwipe: true
                    edge: Common.BottomSwipeArea.Edge.Left
                    availableHeight: firstRunWizard.height
                    availableWidth: firstRunWizard.width

                    onTriggered: {
                        pageHeader.triggerLeftFromBottom()
                        Common.Haptics.play()
                    }

                    Loader {
                        active: firstRunWizard.currentStage == 1
                        asynchronous: true
                        height: units.gu(2)
                        anchors {
                            left: parent.left
                            right: parent.right
                            bottom: parent.bottom
                        }

                        sourceComponent: edgeHintComponent
                    }
                }
                
                Item {
                    id: horizontalSwipeItem

                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    Layout.alignment: Qt.AlignBottom

                    MouseArea {
                        id: bottomEdgeHint

                        readonly property alias color: recVisual.color

                        hoverEnabled: true
                        height: units.gu(0.5)

                        anchors {
                            left: parent.left
                            right: parent.right
                            bottom: parent.bottom
                            bottomMargin: units.gu(0.5)
                        }

                        Behavior on opacity { UbuntuNumberAnimation {} }

                        Rectangle {
                            id: recVisual

                            color: bottomEdgeHint.containsMouse ? UbuntuColors.silk : UbuntuColors.ash
                            radius: height / 2
                            height: bottomEdgeHint.containsMouse ? units.gu(1) : units.gu(0.5)
                            anchors {
                                verticalCenter: parent.verticalCenter
                                left: parent.left
                                right: parent.right
                            }
                            
                            Loader {
                                active: firstRunWizard.currentStage == 2
                                asynchronous: true
                                anchors.fill: parent

                                sourceComponent: edgeHintComponent
                                onLoaded: item.radius = recVisual.radius
                            }
                        }
                    }

                    Common.HorizontalSwipeHandle {
                        id: bottomBackForwardHandle

                        Component.onCompleted: {
                            goBackIcon.swipeProgress = Qt.binding( function() { return swipeProgress } )
                            goForwardIcon.swipeProgress = Qt.binding( function() { return swipeProgress } )
                        }
                        leftAction: goBackIcon
                        rightAction: goForwardIcon
                        immediateRecognition: true
                        usePhysicalUnit: true
                        height: units.gu(2)
                        swipeHoldDuration: 700
                        anchors {
                            left: parent.left
                            right: parent.right
                            bottom: parent.bottom
                        }

                        rightSwipeHoldEnabled: false
                        leftSwipeHoldEnabled: false

                        onRightSwipe:  pageHeader.triggerLeftFromBottom()
                        onLeftSwipe:  pageHeader.triggerRightFromBottom()
                        onPressedChanged: if (pressed) Common.Haptics.playSubtle()
                    }
                }

                Common.BottomSwipeArea {
                    Layout.fillHeight: true
                    Layout.fillWidth: !horizontalSwipeItem.visible
                    Layout.preferredWidth: bottomRowLayout.sideSwipeAreaWidth
                    Layout.alignment: Qt.AlignBottom | Qt.AlignRight

                    model: pageHeader.rightActions
                    enableQuickActions: true
                    triggerSignalOnQuickSwipe: true
                    edge: Common.BottomSwipeArea.Edge.Right
                    availableHeight: firstRunWizard.height
                    availableWidth: firstRunWizard.width

                    onTriggered: {
                        pageHeader.triggerRightFromBottom()
                        Common.Haptics.play()
                    }

                    Loader {
                        active: firstRunWizard.currentStage == 1
                        asynchronous: true
                        height: units.gu(2)
                        anchors {
                            left: parent.left
                            right: parent.right
                            bottom: parent.bottom
                        }

                        sourceComponent: edgeHintComponent
                    }
                }
            }
        }
    }
}
