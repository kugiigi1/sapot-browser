import QtQuick 2.12
import Ubuntu.Components 1.3
import QtQuick.Controls 2.12 as QQC2
import QtWebEngine 1.5
import QtQuick.Layouts 1.12
import "TextUtils.js" as TextUtils

DialogWithContents {
    id: downloadExistsDialog

    objectName: "confirmDialog"

    readonly property string elidedUrl: TextUtils.elideText(url, 200)

    property string url: downloadObj ? downloadObj.url : ""
    property string filename: downloadObj ? downloadObj.filename : ""
    property var downloadObj

    signal allow(var download)
    signal viewDownloads
    signal deny

    title: i18n.tr("File already exists")
    closePolicy: QQC2.Popup.NoAutoClose
    destroyOnClose: true

    onAllow: close()
    onViewDownloads: close()
    onDeny: close()

    QQC2.Label {
        text: i18n.tr("The file (%1) you are trying to download already exists in your Downloads").arg(downloadExistsDialog.filename)
        Layout.fillWidth: true
        wrapMode: Text.WordWrap
    }

    DialogCaption {
        Layout.fillWidth: true
        text: elidedUrl
    }

    ColumnLayout {
        spacing: units.gu(2)
        Layout.fillWidth: true

        Button {
            id: saveButton
            objectName: "allowButton"

            Layout.fillWidth: true
            text: i18n.tr("Download anyway")
            color: theme.palette.normal.positive

            onClicked: downloadExistsDialog.allow(downloadExistsDialog.downloadObj)
        }

        Button {
            objectName: "viewDownloadsButton"

            Layout.fillWidth: true
            enabled: visible
            text: i18n.tr("View downloads")

            onClicked: downloadExistsDialog.viewDownloads()
        }

        Button {
            objectName: "cancelButton"

            Layout.fillWidth: true
            enabled: visible
            text: i18n.tr("Cancel download")

            onClicked: downloadExistsDialog.deny()
        }
    }
}
