import QtQuick 2.12

QtObject {
    property string pageId
    property var pageComponent
    property string iconName
    property string title
    property string description
    property string shortcutText
}
