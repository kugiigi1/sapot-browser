getVideoInfo = function () {
    var videoDiv = document.fullscreenElement
    if (videoDiv) {
        var videos = videoDiv.querySelectorAll("video");
        var videoWidth
        var videoHeight
        var videoOrientation

        for (video of videos) {
            videoWidth = video.videoWidth
            videoHeight = video.videoHeight

            if (videoWidth > videoHeight) {
                videoOrientation = "landscape"
            } else {
                videoOrientation = "portrait"
            }
        }
        console.log('{"type": "VIDEO", "orientation": "' + videoOrientation + '"}' )
    }
}

document.addEventListener('fullscreenchange', getVideoInfo);
